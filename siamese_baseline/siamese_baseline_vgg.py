

from __future__ import print_function, division
from loss import huber_loss,HuberLoss
import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
from torch.autograd import Variable
import numpy as np
import torchvision
from torch.utils.data import Dataset, DataLoader
from torchvision import datasets, models, transforms
import matplotlib.pyplot as plt
import time
import os
import copy
import re
import pandas as pd
import sys
from skimage import io,transform
from PIL import Image
from resnet_baseline import resnet18
import torch.nn.functional as F
from tensorboardX import SummaryWriter
from vgg import vgg16_bn

#from torchviz import make_dot
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="1,0"
plt.ion()   # interactive mode
batch_size=16
dtype = torch.cuda.FloatTensor
dtype_long = torch.cuda.LongTensor
writer=SummaryWriter()
# model_ft=resnet18(pretrained=True)
# torch.save(model_ft,'./model_ft_30PReLU2.pth')
# sys.exit(0)


class siamese_baseline(nn.Module):
    def __init__(self,params=None,num_class=4,source_size=(14,14),target_size=(14,28),batch_size=4):
        super(siamese_baseline,self).__init__()
        self.params=params
        self.source_size=source_size
        self.target_size=target_size
# Set up params to load the resnet18 weights from places dataset       
        self.num_class=num_class
        self.batch_size=batch_size
        # self.pool=nn.AvgPool2d(kernel_size=(2,2),stride=2)
        # model_ft=resnet18(pretrained=True,hyper_column=False,no_fc=True)

        model_ft=vgg16_bn(pretrained=True)
        self.model_ft=nn.Sequential(*list(model_ft.features.children())[:-12])
        self.pool22=nn.AvgPool2d(kernel_size=(15,15),stride=2)
        self.pool12=nn.AvgPool2d(kernel_size=(5,5),stride=6)
        self.pool5=nn.AvgPool2d(kernel_size=(17,17),stride=12)

        for p in model_ft.parameters():
            p.bias=True
        



#         # model_ft.fc=nn.Linear(512,256)
        self.model_ft2=self.model_ft
#         # model_ft2.fc=nn.Linear(4096,256)
        self.fc_a=nn.Linear(512,256)

#         # self.fc_s=nn.Linear(4096,1024)
#         self.fc_s2=nn.Linear(512,256)
        
#         self.model_ft=model_ft
#         self.model_ft2=model_ft2

# #hyper input needs to be replaced        
#         self.pool1=nn.AvgPool2d(kernel_size=(7,7),stride=1)
#         self.pool2=nn.AvgPool2d(kernel_size=(7,14),stride=1)

        self.conv1=nn.Sequential(nn.Conv2d(960,512,kernel_size=(1,1)),nn.BatchNorm2d(512),nn.PReLU())
        self.conv2=nn.Sequential(nn.Conv2d(512,512,kernel_size=(1,1)),nn.BatchNorm2d(512),nn.PReLU())
        self.conv3=nn.Conv2d(512,self.num_class,(1,1))

        # self.conv1st=nn.Sequential(nn.Conv2d(960,512,kernel_size=(1,1)),nn.BatchNorm2d(512),nn.PReLU())
        # self.conv2st=nn.Sequential(nn.Conv2d(512,512,kernel_size=(1,1)),nn.BatchNorm2d(512),nn.PReLU())
        # self.conv3st=nn.Conv2d(512,self.num_class,(1,1))        




    def forward(self,aerial_view,street_view):
        
        print(aerial_view.shape)
        
        
        # x1,HyperColumn1=self.model_ft(aerial_view)
        # x2,HyperColumn2=self.model_ft2(street_view)
        x1=aerial_view
        x2=street_view
        module_list=list(self.model_ft)
        for i,layer in enumerate(module_list):
            print(i)
            x1=layer(x1)
            print(x1.shape)
            if(i==5):
                # print(x1.shape)
                # h1=x1
                h1=self.pool5(x1)
                print(x1.shape)
            if(i==12):
                # print(x1.shape)

                # h2=x1
                h2=self.pool12(x1)
                print(x1.shape)
            if(i==22):
                # print(x1.shape)
                h3=self.pool22(x1)
                print(x1.shape)



        

        # x2=self.model_ft2(street_view)

        # x1=self.pool1(x1)
        
        # x1=self.pool1(x1)
        print(x1.shape)
        sys.exit(0)
        print(str(self.model_ft))
        HyperColumn=torch.cat([h1,h2,h3,x1],dim=1)
        x1=self.conv1(HyperColumn1)
        x1=self.conv2(x1)
        x1=self.conv3(x1)

        
        # x1=x1.view((x1.shape[0],-1))
        # x1=self.fc_a(x1)

        # x2=self.conv1(HyperColumn2)
        # x2=self.conv2(x2)
        # x2=self.pool2(x2)
        
        # x2=self.pool2(x2)
        # print(x2.shape)
        # x2=x2.view(x2.shape[0],-1)
        # # x2=self.fc_s(x2)
        # x2=self.fc_s2(x2)
        
        print('aerial view is {} {}'.format(x1.shape,x2.shape))
        
        


        

        print('shapes of the aerial and street view feature maps {} {}'.format(x1.shape,x2.shape))
        return x1,x2

def bilinear_interpolate_torch(im, x, y):
    x0 = torch.floor(x).type(dtype_long)
    x1 = x0 + 1
    
    y0 = torch.floor(y).type(dtype_long)
    y1 = y0 + 1

    x0 = torch.clamp(x0, 0, im.shape[1]-1)
    x1 = torch.clamp(x1, 0, im.shape[1]-1)
    y0 = torch.clamp(y0, 0, im.shape[0]-1)
    y1 = torch.clamp(y1, 0, im.shape[0]-1)
    
    Ia = im[ y0, x0 ][0]
    Ib = im[ y1, x0 ][0]
    Ic = im[ y0, x1 ][0]
    Id = im[ y1, x1 ][0]
    
    wa = (x1.type(dtype)-x) * (y1.type(dtype)-y)
    wb = (x1.type(dtype)-x) * (y-y0.type(dtype))
    wc = (x-x0.type(dtype)) * (y1.type(dtype)-y)
    wd = (x-x0.type(dtype)) * (y-y0.type(dtype))

    return torch.t((torch.t(Ia)*wa)) + torch.t(torch.t(Ib)*wb) + torch.t(torch.t(Ic)*wc) + torch.t(torch.t(Id)*wd)
# gg=torch.rand((1,3,9,9))
# # x=torch.
# # .Tensor(1,2)
# # print(x)
# # x[0,0],x[0,1]=3.0,3.0
# # x=x.type(torch.FloatTensor)
# W, H, C = 25, 25, 7
# image = np.random.randn(C,W, H)
# image=image.transpose(1,2,0)


# num_samples = 4
# samples_x, samples_y = np.random.rand(num_samples)*(W-1), np.random.rand(num_samples)*(H-1)



# image = torch.from_numpy(image).type(dtype)
# samples_x = torch.FloatTensor([samples_x]).type(dtype)
# samples_y = torch.FloatTensor([samples_y]).type(dtype)

# print (bilinear_interpolate_torch(image, samples_x, samples_y))
# sys.exit(0)
def calc_mean_std(dataset):

    r_a,r_s=0.0,0.0
    g_a,g_s=0.0,0.0
    b_a,b_s=0.0,0.0
    dataLoader=torch.utils.data.DataLoader(data_train,batch_size=1,num_workers=4)
    for i,data in enumerate(dataLoader):
        inputs1,inputs2=data['aerial_image'],data['street_image']
        inputs1=Variable(inputs1).cuda()
        # inputs2=Variable(inputs2).cuda()
        
        r_a+=torch.sum(inputs1[0,0,:,:])
        g_a+=torch.sum(inputs1[0,1,:,:])
        b_a+=torch.sum(inputs1[0,2,:,:])

        r_s+=torch.sum(inputs2[0,0,:,:])
        g_s+=torch.sum(inputs2[0,1,:,:])
        b_s+=torch.sum(inputs2[0,2,:,:])


    aerial,street=data_train[0]['aerial_image'],data_train[0]['street_image']
    print(aerial.shape)
    H_a,W_a=aerial.shape[1:]
    H_s,W_s=street.shape[1:]
 
    mean_ra=r_a/(len(dataset)*H_a*W_a)
    mean_ga=g_a/(len(dataset)*H_a*W_a)
    mean_ba=b_a/(len(dataset)*H_a*W_a)

    mean_rs=r_s/(len(dataset)*H_s*W_s)
    mean_gs=g_s/(len(dataset)*H_s*W_s)
    mean_bs=b_s/(len(dataset)*H_s*W_s)

    std_ra,std_ga,std_ba=0.0,0.0,0.0
    std_rs,std_gs,std_bs=0.0,0.0,0.0
    for i,data in enumerate(dataLoader):
        inputs1,inputs2=data['aerial_image'],data['street_image']
        inputs1=Variable(inputs1).cuda()
        inputs2=Variable(inputs2).cuda()
        std_ra+=torch.sum((inputs1[0,0,:,:]-mean_ra)**2)
        std_ga+=torch.sum((inputs1[0,1,:,:]-mean_ga)**2)
        std_ba+=torch.sum((inputs1[0,2,:,:]-mean_ba)**2)


        std_rs+=torch.sum((inputs2[0,0,:,:]-mean_rs)**2)
        std_gs+=torch.sum((inputs2[0,1,:,:]-mean_gs)**2)
        std_bs+=torch.sum((inputs2[0,2,:,:]-mean_bs)**2)


    std_ra,std_ga,std_ba=torch.sqrt((std_ra)/((H_a*W_a*len(dataset))-1)),torch.sqrt((std_ga)/((H_a*W_a*len(dataset))-1)),torch.sqrt((std_ba)/((H_a*W_a*len(dataset))-1))
    std_rs,std_gs,std_bs=torch.sqrt((std_rs)/((H_s*W_s*len(dataset))-1)),torch.sqrt((std_gs)/((H_s*W_s*len(dataset))-1)),torch.sqrt((std_bs)/((H_s*W_s*len(dataset))-1))

    mean_a=[mean_ra,mean_ga,mean_ba]

    mean_s=[mean_rs,mean_gs,mean_bs]

    std_a=[std_ra,std_ga,std_ba]
    std_s=[std_rs,std_gs,std_bs]

    return mean_a,mean_s,std_a,std_s
        



class AerialDataset(Dataset):
    """Face Landmarks dataset."""

    def __init__(self, csv_file, root_dir, transform=None):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a batch.
        """
        self.data_frame = pd.read_csv(csv_file)
        self.root_dir = root_dir
        self.transform = transform

    def __len__(self):
        return len(self.data_frame)

    def __getitem__(self, idx):
        aerial_img_name = os.path.join(self.root_dir,
                                self.data_frame.iloc[idx, 0])
        aerial_image = io.imread(aerial_img_name)
        aerial_image=Image.fromarray(np.uint8(aerial_image))
        # aerial_image=np.transpose(aerial_image,(2,0,1))
        street_image_name=os.path.join(self.root_dir,self.data_frame.iloc[idx,1])
        print(aerial_img_name,street_image_name)
        street_image=io.imread(street_image_name)
        street_image=Image.fromarray((street_image[:,:,:3]))
# CHANGE street_image to convert RGBA to RGB
        # street_image=np.transpose(street_image,(2,0,1))
        

        if self.transform:
            aerial_image=self.transform[0](aerial_image)
            street_image=self.transform[1](street_image)
        batch={'aerial_image':aerial_image,'street_image':street_image}
        # 
        return batch


data_transforms = {
    'aerial_image': transforms.Compose([
        transforms.Resize((224,224)),
        transforms.ToTensor()
        ,transforms.Normalize([.3802,.3770,.3493], [.1999,.1997,.1928])
    ]),
    'street_image': transforms.Compose([
        transforms.Resize(640,640),
        transforms.CenterCrop(640),
        transforms.ToTensor()
        
    ]),
}

data_train=AerialDataset('../aerial/data_op_training.csv','../aerial',transform=[data_transforms['aerial_image'],    transforms.Compose([transforms.Resize((224,448)),transforms.ToTensor(),transforms.Normalize([.4881,.5005,.5113], [.1888,.1843,.1968])])])
# a,b,c,d=calc_mean_std(data_train)

data_test=AerialDataset('../aerial/data_op_val.csv','../aerial',transform=[data_transforms['aerial_image'],transforms.Compose([transforms.Resize((224,448)),transforms.ToTensor(),transforms.Normalize([.4881,.5005,.5113], [.1888,.1843,.1968])])])
data_test_actual=AerialDataset('../aerial/data_op_test.csv','../aerial',transform=[data_transforms['aerial_image'],transforms.Compose([transforms.Resize((224,448)),transforms.ToTensor(),transforms.Normalize([.4881,.5005,.5113], [.1888,.1843,.1968])])])
dataLoader_train=torch.utils.data.DataLoader(data_train,batch_size=batch_size,shuffle=True,num_workers=4)
dataLoader_test=torch.utils.data.DataLoader(data_test,batch_size=batch_size,shuffle=True,num_workers=4)

dataLoader_actual_test=torch.utils.data.DataLoader(data_test_actual,batch_size=batch_size,shuffle=True,num_workers=4)
dataloaders={'train':dataLoader_train,'val':dataLoader_actual_test}




use_gpu = torch.cuda.is_available()


def imshow(inp, title=None):
    """Imshow for Tensor."""
    inp = inp.numpy().transpose((1, 2, 0))
    mean = np.array([0.485, 0.456, 0.406])
    std = np.array([0.229, 0.224, 0.225])
    inp = std * inp + mean
    inp = np.clip(inp, 0, 1)
    plt.imshow(inp)
    if title is not None:
        plt.title(title)
    plt.pause(3)  # pause a bit so that plots are updated


# Get a batch of training data
# inputs, classes = next(iter(dataloaders['train']))

# # Make a grid from batch
# out = torchvision.utils.make_grid(inputs)

# imshow(out, title=[class_names[x] for x in classes])


######################################################################
# Training the model
# ------------------
#
# Now, let's write a general function to train a model. Here, we will
# illustrate:
#
# -  Scheduling the learning rate
# -  Saving the best model
#
# In the following, parameter ``scheduler`` is an LR scheduler object from
# ``torch.optim.lr_scheduler``.

# print(len(data_train))
# 

def train_model(model,criterion, optimizer, scheduler, num_epochs=25):
    since = time.time()

    best_model_wts = copy.deepcopy(model.state_dict())
    best_acc = 9999.99
    iters=0
    flag=1
    for epoch in range(num_epochs):
        print('Epoch {}/{}'.format(epoch, num_epochs - 1))
        print('-' * 10)

        # Each epoch has a training and validation phase
        for phase in ['train', 'val']:
            if phase == 'train':
                scheduler.step()
                model.train(True)  # Set model to training mode
            else:
                model.eval()  # Set model to etestuate mode

            running_loss = 0.0
            running_corrects = 0

            epoch_loss=999.99
            iters=len(dataloaders[phase])*epoch*batch_size
#Part of actual validation code            
            # aerial_list=[]
            # street_list=[]
            # weights1=np.
            # Iterate over data.
            for i,data in enumerate(dataloaders[phase]):
                # get the inputs
                inputs, labels = data['aerial_image'],data['street_image']
                print(inputs.shape,labels.shape)
                print(i)
                running_loss_val=0
                # wrap them in Variablex
                # print(use_gpu)
                if use_gpu:
                    inputs = Variable(inputs.cuda())
                    
                    
                    labels = Variable(labels.cuda())
                    # if(flag==1):
                    #     # c,d=model(inputs,labels)
                    #     # writer.add_graph_onnx(model)
                    #     c,d=model(inputs,labels)
                    #     k=make_dot(c,params=dict(model.named_parameters()))
                    #     io.imsave('hel.png',k)
                    #     a=input('cont?')
                    #     flag=0
                else:
                    inputs, labels = Variable(inputs), Variable(labels)

                if(inputs.shape[0]!=batch_size):
                    continue
                # zero the parameter gradients
                optimizer.zero_grad()

                # forward
                outputs,outputs2 = model(inputs,labels)
                print('aerial outputs {}'.format(outputs))
# 
                # a=input('dfgdfg')

                print('streeet outputs {}'.format(outputs2))
                # a=input('dfgdfg')
                loss=criterion(outputs,outputs2)


                # backward + optimize only if in training phase
                if phase == 'train':
                    loss.backward()
                    # if(flag==0):
                    #     for i,j in model.named_parameters():
                    #         if(j.grad is None):
                    #             j.requires_grad=False
                # else:
                #     aerial_list.append(outputs)
                #     street_list.append(outputs2)

                    torch.nn.utils.clip_grad_norm(model.parameters(),2.)
                    
                    # for p,j in model.named_parameters():
                        

                    #     # print('here {}'.format(j.grad.data))
                        
                    #     # print(p.data)
                    #     if(j.grad is not None):
                    #         j.data.add_(-0.00001,j.grad.data)
                    
                    optimizer.step()
                # statistics
                iteration_loss=0

             
                iteration_loss=loss.data[0]

                running_loss += iteration_loss
                writer.add_scalar('data/loss',iteration_loss,i+iters)
                # running_corrects += torch.sum(preds == labels.data)

                print('Loss: {}'.format(iteration_loss))
                print('running loss {}'.format(running_loss))

           
                                 
            
            # epoch_acc = running_corrects / dataset_sizes[phase]
            if phase=='train':
                epoch_loss = running_loss / len(data_train)
                writer.add_scalar('data/epoch_loss_train',epoch_loss,i+iters)
            else:
                epoch_loss = running_loss / len(data_test_actual)
                writer.add_scalar('data/epoch_loss_validation',epoch_loss,i+iters)
            for name,param in model.named_parameters():


                writer.add_histogram(name,param,i+iters)            
            print('{} Loss: {} Acc: '.format(
                phase, epoch_loss))

            print(running_loss)
            print('hee')
            print(iteration_loss,epoch_loss,best_acc)

            # deep copy the model
            print(i,len(data_test_actual)/batch_size)
            # if(i>=(len(data_test_actual)-batch_size)/batch_size and phase=='val'):
            #     print('the epoch is {}'.format(epoch))
            #     a=input('here?')
            if phase=='val' and epoch_loss < best_acc and i>=(len(data_test_actual)-batch_size)/batch_size:
                # cc=input('cont?')
                best_acc = epoch_loss
                best_model_wts = copy.deepcopy(model.state_dict())
                    
    writer.export_scalars_to_json('./all_scalars.json')
    writer.close()
    
 #Actual validation code (don't foregt the aerial_list,street_list above)

            # if phase=='val' and len(aerial_list)==len(dataloaders['val']):
            #     # with torch.cpu():
            #     print('cont?')
            #     a=input()
            #     aerial_matrix=torch.stack(aerial_list,dim=0).cpu()
            #     street_matrix=torch.stack(street_list,dim=0).cpu()
            #     distance_matrix=pairwise_distances(aerial_matrix,street_matrix)
            #     idx=distance_matrix.min(dim=1)
            #     for i in range(0,len(idx[1])):
            #         print(idx[1][i],i)
            #         # a=input(('cont?'))
            #         if(idx[1][i]==i):
            #             print(i,idx[1][i])
            #             correct+=1

            #     print('correct ones {}'.format(correct))
            #     print('total number {}'.format(len(idx[1])))
            #     print('top-1 accuracy {}'.format(correct/len(idx[1])))  
            #     aerial_list=[]
            #     street_list=[]              
            #     if (best_acc<(correct/len(idx[1]))):
            #         best_acc = correct/len(idx[1])
            #         best_model_wts =copy.deepcopy(model.state_dict())

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(
        time_elapsed // 60, time_elapsed % 60))
    # print('Best val Acc: {:4f}'.format(best_acc))

    # load best model weights
    # best_model_wts=copy.deepcopy(model.state_dict())
    # model.load_state_dict(best_model_wts)
    model.load_state_dict(best_model_wts)

    return model
# with torch.cuda.device(1):

path='../whole_resnet18_places365_python36.pth.tar'
# visualize_model(model_ft)
model_ftq=torch.load(path)
params={i:j for i,j in model_ftq.named_parameters() }
# print(params.keys())
paramsi=dict(params)
for i in paramsi.keys():
    if('fc' in i):
        del params[i]


#Pass params when possible to load places' weights
model_ft=siamese_baseline(batch_size=batch_size)
model_ft=model_ft.cuda()

# model_ft=vgg16_bn(pretrained=True)
# model_ft=nn.Sequential(*list(model_ft.features.children())[:-12])
# model=torch.load('./model_ft_30PReLU.pth')
# model=model.cuda()
iters=iter(dataLoader_train)
k=iters.next()
image,label=k['aerial_image'],k['street_image']
print(image.shape,label.shape)
criterion=HuberLoss(10.0)
image=Variable(image.cuda())
label=Variable(label.cuda())
x,y=model_ft(image,label)
# Observe that all parameters are being optimized
# print({ i:j for i,j in model.named_parameters()}.keys())

sys.exit(0)

optimizer_ft = optim.RMSprop(model.parameters(), lr=0.0001, momentum=0.9,  weight_decay=.0005)


# Decay LR by a factor of 0.1 every 7 epochs
exp_lr_scheduler = lr_scheduler.StepLR(optimizer_ft, step_size=100, gamma=0.1)

######################################################################
# Train and evaluate
# ^^^^^^^^^^^^^^^^^^
#
# It should take around 15-25 min on CPU. On GPU though, it takes less than a
# minute.
#



# print(model.modules())
# dicts={i:j for i,j in model.named_parameters()}
# print(dicts)
# print(dicts.keys())
# print(model_ft.layer4)

model_ft = train_model(model, criterion, optimizer_ft, exp_lr_scheduler,
                       num_epochs=10)




# torch.save(model_ft,'./model_ft_imagenet.pth')
torch.save(model_ft,'./model_ft_30PReLU30.pth')
model_ft.eval()
sys.exit(0)



running_loss=0
for i,data in enumerate(dataLoader_actual_test):
    inputs,targets=data['aerial_image'],data['street_image']
    if(use_gpu==True):
        inputs=Variable(inputs.cuda())
        targets=Variable(targets.cuda())
    outputs1,outputs2=model_ft(inputs,targets)
    
#REPLACE huber_loss with l2 distance   
    
    # outputs1=outputs1.unsqueeze(dim=1)
    # outputs2=outputs2.unsqueeze(dim=1)
    
    # print(outputs1.shape,outputs2.shape)
    
    outputs1=outputs1.view((batch_size,outputs1.shape[1]*outputs1.shape[2]) )
    outputs2=outputs2.view((batch_size,outputs2.shape[1]*outputs2.shape[2]))
    # outputs1=torch.unsqueeze(outputs1,dim=1)
    # outputs2=torch.unsqueeze(outputs2,dim=1)
    loss=Variable(torch.FloatTensor(1,batch_size).zero_())
    
    # for i in range(0,batch_size):
    #     print(outputs1[i].shape,outputs2[i].shape)
    #     loss[i]=F.pairwise_distance(outputs1[i],outputs2[i])
    # print(outputs1.shape,outputs2.shape)
    loss=F.pairwise_distance(outputs1,outputs2)
    print(loss.data)

    

    iteration_loss=loss.data[0]*batch_size
    print(iteration_loss.shape)
    
    running_loss+=iteration_loss

    # print('loss: {:.4f}'.format(iteration_loss))

    # print(i)
    

print(running_loss/len(data_test_actual))
# print('epoch loss_test :{:.4f}'.format(running_loss/len(data_test_actual)))

######################################################################
#
sys.exit(0)

visualize_model(model_ft)


######################################################################
# ConvNet as fixed feature extractor
# ----------------------------------
#
# Here, we need to freeze all the network except the final layer. We need
# to set ``requires_grad == False`` to freeze the parameters so that the
# gradients are not computed in ``backward()``.
#
# You can read more about this in the documentation
# `here <http://pytorch.org/docs/notes/autograd.html#excluding-subgraphs-from-backward>`__.
#

model_conv = torchvision.models.resnet18(pretrained=True)
for param in model_conv.parameters():
    param.requires_grad = False

# Parameters of newly constructed modules have requires_grad=True by default
num_ftrs = model_conv.fc.in_features
model_conv.fc = nn.Linear(num_ftrs, 2)

if use_gpu:
    model_conv = model_conv.cuda()

criterion = nn.CrossEntropyLoss()

# Observe that only parameters of final layer are being optimized as
# opoosed to before.
optimizer_conv = optim.SGD(model_conv.fc.parameters(), lr=0.001, momentum=0.9)

# Decay LR by a factor of 0.1 every 7 epochs
exp_lr_scheduler = lr_scheduler.StepLR(optimizer_conv, step_size=7, gamma=0.1)


######################################################################
# Train and evaluate
# ^^^^^^^^^^^^^^^^^^
#
# On CPU this will take about half the time compared to previous scenario.
# This is expected as gradients don't need to be computed for most of the
# network. However, forward does need to be computed.
#

model_conv = train_model(model_conv, criterion, optimizer_conv,
                         exp_lr_scheduler, num_epochs=10)

######################################################################
#

visualize_model(model_conv)

plt.ioff()
plt.show()
