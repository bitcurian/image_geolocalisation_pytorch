import torch
from torch.autograd import Variable
import numpy as np
import torch.nn as nn
import sys
import torch.nn.functional as F
import math
def huber_loss(source, target,threshold=1.0):
	error=torch.abs(source-target)
	linear=threshold*(error-.5*threshold)
	quadratic=.5*error*error

	print(source.shape,target.shape)
	# sys.exit(0)
	return torch.sum(where(error<threshold,quadratic,linear))

def where(cond, x_1, x_2):

    cond = cond.float()    
    
    # print(cond.is_cuda,x_1.is_cuda,x_2.is_cuda)
    # sys.exit(0)
    return (cond * x_1) + ((1-cond) * x_2)


class HuberLoss(nn.Module):

    def __init__(self, margin=1.0):
        super(HuberLoss, self).__init__()
        self.margin = margin

    def forward(self, source,target):
        
        error=torch.abs(source-target)
        quad=0.5*error*error
        linear=self.margin*(error-.5*self.margin)

        return torch.sum(where(error<self.margin,quad,linear))

class DBLContrastiveLoss(nn.Module):

    def __init__(self, margin=1.0):
        super(DBLContrastiveLoss, self).__init__()
        self.margin1=margin
        # m=np.array(margin)
        # m=torch.from_numpy(m)
        
        # print (type(m.float()))
        # self.margin = Variable(m.float(),requires_grad=False)
        # self.margin=

    def forward(self, source,target,label):
        euclidean_distance = F.pairwise_distance(source, target)
        self.margin=np.full((euclidean_distance.shape),self.margin1,dtype=np.float)
        self.margin=torch.from_numpy(self.margin)
        self.margin=Variable(self.margin.float(),requires_grad=False).cuda()

        p=(1+torch.exp(-self.margin))/(1+torch.exp(euclidean_distance-self.margin))
        # loss_contrastive = torch.mean((1-label) * torch.pow(euclidean_distance, 2) +
        #                               (label) * torch.pow(torch.clamp(self.margin - euclidean_distance, min=0.0), 2))

        # print(type(torch.FloatTensor(label.data)),type(p.data))
        # print(type(torch.LongTensor(label)),type(p.data))        
        loss_contrastive = torch.mean(-((1-label.float()) * torch.log(1-p)+(label.float()) * torch.log(p))) 
                                      
        return loss_contrastive