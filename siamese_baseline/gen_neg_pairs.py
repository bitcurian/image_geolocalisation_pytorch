import re 
import sys
import linecache
import random
import math
def dist_gps_coordinates(coord1,coord2):

    lat_p=re.compile('.*?(?=_)')
    lon_pattern=re.compile('_([0-9\-\.]*)')
    lat1=float(re.search(lat_p,coord1).group())
    lon1=float(re.search(lon_pattern,coord1).group(1))
    lat2=float(re.search(lat_p,coord2).group())
    lon2=float(re.search(lon_pattern,coord2).group(1))
    print(lat1,lon1)
    print(lat2,lon2)
    
    d=2*math.asin(math.sqrt((math.sin((lat1-lat2)/2))**2 + 
                 math.cos(lat1)*math.cos(lat2)*(math.sin((lon1-lon2)/2))**2))
    return d*10**5


def main():
#ratio for contrastive loss
	ratio='1:5'
	ratio=int(ratio.split(':')[1])
	# pattern=re.compile('/[0-9_\.\-]*?(?=\.png)')
	# print(re.search(pattern,'\xef\xbb\xbfaerial2/nadir_all/pittsburg_nadir/40.444_-80.0027_nadir.png').group())

	filename='../aerial/data_op_test.csv'
#generate contrastive pairs and store them in writefile	
	writefile='../aerial/data_op_test_c.csv'
	f=open(filename,'r+')
	f1=open(writefile,'w+')
	c=f.readline()

	total_lines=len(f.readlines())
	f.seek(0,0)
	i=0

	while(c):
		distance=0
		r = [name for name in c.split(',')]
		r[1]=r[1].translate(None,'\n')
		string2=r[0]+','+r[1]+','+str(1)+'\n'
		f1.write(string2)
		# r[2]=r[2].translate(None,'\n')
	
		# n=((f.tell()+100)%total_lines)+1
		iteration=0

		while(iteration<ratio):
			n=(f.tell()+random.randint(0,total_lines))%total_lines+1
			try:
				k=linecache.getline(filename,n)
				# requires better solution
				
				k=[name for name in k.split(',')]
				# k[2]=k[2].translate(None,'\n')
				coord1=r[0].split('/')[-1].replace('_nadir.png','')
				coord2=k[0].split('/')[-1].replace('_nadir.png','')
				k[1]=k[1].translate(None,'\n')				
				distance=dist_gps_coordinates(coord1,coord2)
				if(distance<350):
					continue
				string1=r[0]+','+k[1]+','+str(0)+'\n'

				# string1=r[0]+','+r[2]+'\n'

			except Exception as e:
				print(e)
				print(r,k,n)
				# c=f.readline()
				n=random.randint(0,total_lines)
	#replace with better error handling solution			
				a=input('error')
				continue
				
		#	print (string,n)
		#	sys.exit(0)
		#	print(f.tell(),type(f.tell()))
			#offset=f.tell()
			#f.seek(0,2)
			f1.write(string1)
			iteration+=1
			print(iteration,ratio)

	#	f1.write(string)
		#f.seek(offset,0)
		# requires better method to write at the file's end
		c=f.readline()
		i+=1
		print (i*ratio)
		
	
	print('exit')

if __name__=="__main__":
	main()