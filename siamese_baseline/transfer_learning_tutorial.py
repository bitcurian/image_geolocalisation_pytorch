# -*- coding: utf-8 -*-
"""
Transfer Learning tutorial
==========================
**Author**: `Sasank Chilamkurthy <https://chsasank.github.io>`_

In this tutorial, you will learn how to train your network using
transfer learning. You can read more about the transfer learning at `cs231n
notes <http://cs231n.github.io/transfer-learning/>`__

Quoting this notes,

    In practice, very few people train an entire Convolutional Network
    from scratch (with random initialization), because it is relatively
    rare to have a dataset of sufficient size. Instead, it is common to
    pretrain a ConvNet on a very large dataset (e.g. ImageNet, which
    contains 1.2 million images with 1000 categories), and then use the
    ConvNet either as an initialization or a fixed feature extractor for
    the task of interest.

These two major transfer learning scenarios looks as follows:

-  **Finetuning the convnet**: Instead of random initializaion, we
   initialize the network with a pretrained network, like the one that is
   trained on imagenet 1000 dataset. Rest of the training looks as
   usual.
-  **ConvNet as fixed feature extractor**: Here, we will freeze the weights
   for all of the network except that of the final fully connected
   layer. This last fully connected layer is replaced with a new one
   with random weights and only this layer is trained.

"""
# License: BSD
# Author: Sasank Chilamkurthy

from __future__ import print_function, division

import sys
from loss import HuberLoss
import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
from torch.autograd import Variable
import numpy as np
import torchvision
from torchvision import datasets, models, transforms
import matplotlib.pyplot as plt
import time
import os
import copy
from torch.utils.data import DataLoader,Dataset
import pandas as pd
from skimage import io,transform
from resnet_original import resnet18
from PIL import Image
plt.ion()   # interactive mode
batch_size=16
######################################################################
# Load Data
# ---------
#
# We will use torchvision and torch.utils.data packages for loading the
# data.
#
# The problem we're going to solve today is to train a model to classify
# **ants** and **bees**. We have about 120 training images each for ants and bees.
# There are 75 validation images for each class. Usually, this is a very
# small dataset to generalize upon, if trained from scratch. Since we
# are using transfer learning, we should be able to generalize reasonably
# well.
#
# This dataset is a very small subset of imagenet.
#
# .. Note ::
#    Download the data from
#    `here <https://download.pytorch.org/tutorial/hymenoptera_data.zip>`_
#    and extract it to the current directory.

# Data augmentation and normalization for training
# Just normalization for validation

class offshelf(nn.Module):

    def __init__(self):
        super(offshelf,self).__init__()

        model=resnet18(pretrained=True)
        for param in model.parameters():
            param.requires_grad = False      
        self.model=model  
        self.pool_a=nn.AvgPool2d(kernel_size=(7,7),stride=1)
        self.pool_s=nn.AvgPool2d(kernel_size=(7,14),stride=1)

        self.fc_a=nn.Linear(512,256)
        self.fc_s=nn.Linear(512,256)

    def forward(self,aerial,street):
        x1=self.model(aerial)
        x1=self.pool_a(x1)
        x1=x1.view(x1.shape[0],-1)
        x1=self.fc_a(x1)

        x2=self.model(street)
        x2=self.pool_s(x2)
        x2=x2.view(x1.shape[0],-1)
        x2=self.fc_s(x2)

        return x1,x2
class AerialDataset(Dataset):
    """Face Landmarks dataset."""

    def __init__(self, csv_file, root_dir, transform=None):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a batch.
        """
        self.data_frame = pd.read_csv(csv_file)
        self.root_dir = root_dir
        self.transform = transform

    def __len__(self):
        return len(self.data_frame)

    def __getitem__(self, idx):
        aerial_img_name = os.path.join(self.root_dir,
                                self.data_frame.iloc[idx, 0])
        aerial_image = io.imread(aerial_img_name)
        aerial_image=Image.fromarray(np.uint8(aerial_image))
        # aerial_image=np.transpose(aerial_image,(2,0,1))
        street_image_name=os.path.join(self.root_dir,self.data_frame.iloc[idx,1])
        street_image=io.imread(street_image_name)
        street_image=Image.fromarray((street_image[:,:,:3]))
# CHANGE street_image to convert RGBA to RGB
        # street_image=np.transpose(street_image,(2,0,1))
        

        if self.transform:
            aerial_image=self.transform[0](aerial_image)
            street_image=self.transform[1](street_image)
        # print(aerial_image,type(aerial_image))
        batch={'aerial_image':aerial_image,'street_image':street_image}
        #
        return batch


data_transforms = {
    'aerial_image': transforms.Compose([
        transforms.Resize((224,224)),
        transforms.ToTensor()
        ,transforms.Normalize([.3802,.3770,.3493], [.1999,.1997,.1928])
    ]),
    'street_image': transforms.Compose([
        transforms.Resize(640,640),
        transforms.CenterCrop(640),
        transforms.ToTensor()
        
    ]),
}

data_train=AerialDataset('../aerial/data_op_training.csv','../aerial',transform=[data_transforms['aerial_image'],    transforms.Compose([transforms.Resize((224,448)),transforms.ToTensor(),transforms.Normalize([.4881,.5005,.5113], [.1888,.1843,.1968])])])
# a,b,c,d=calc_mean_std(data_train)

data_test=AerialDataset('../aerial/data_op_val.csv','../aerial',transform=[data_transforms['aerial_image'],transforms.Compose([transforms.Resize((224,448)),transforms.ToTensor(),transforms.Normalize([.4881,.5005,.5113], [.1888,.1843,.1968])])])
data_test_actual=AerialDataset('../aerial/data_op_test.csv','../aerial',transform=[data_transforms['aerial_image'],transforms.Compose([transforms.Resize((224,448)),transforms.ToTensor(),transforms.Normalize([.4881,.5005,.5113], [.1888,.1843,.1968])])])
dataLoader_train=torch.utils.data.DataLoader(data_train,batch_size=batch_size,shuffle=True,num_workers=4)
dataLoader_test=torch.utils.data.DataLoader(data_test,batch_size=batch_size,shuffle=True,num_workers=4)

dataLoader_actual_test=torch.utils.data.DataLoader(data_test_actual,batch_size=batch_size,shuffle=True,num_workers=4)
dataloaders={'train':dataLoader_train,'val':dataLoader_actual_test}
use_gpu = torch.cuda.is_available()

######################################################################
# Visualize a few images
# ^^^^^^^^^^^^^^^^^^^^^^
# Let's visualize a few training images so as to understand the data
# augmentations.

def imshow(inp, title=None):
    """Imshow for Tensor."""
    inp = inp.numpy().transpose((1, 2, 0))
    mean = np.array([0.485, 0.456, 0.406])
    std = np.array([0.229, 0.224, 0.225])
    inp = std * inp + mean
    inp = np.clip(inp, 0, 1)
    plt.imshow(inp)
    if title is not None:
        plt.title(title)
    plt.pause(0.001)  # pause a bit so that plots are updated


# Get a batch of training data



######################################################################
# Training the model
# ------------------
#
# Now, let's write a general function to train a model. Here, we will
# illustrate:
#
# -  Scheduling the learning rate
# -  Saving the best model
#
# In the following, parameter ``scheduler`` is an LR scheduler object from
# ``torch.optim.lr_scheduler``.

dataset_sizes={'train':len(data_train),'val':len(data_test)}
def train_model(model, criterion, optimizer, scheduler, num_epochs=25):
    since = time.time()

    best_model_wts = copy.deepcopy(model.state_dict())
    best_loss = 99999.99

    for epoch in range(num_epochs):
        print('Epoch {}/{}'.format(epoch, num_epochs - 1))
        print('-' * 10)

        # Each epoch has a training and validation phase
        for phase in ['train', 'val']:
            if phase == 'train':
                scheduler.step()
                model.train(True)  # Set model to training mode
            else:
                model.train(False)  # Set model to evaluate mode

            running_loss = 0.0
            

            # Iterate over data.
            for data in dataloaders[phase]:
                # get the inputs
                inputs, labels = data['aerial_image'],data['street_image']

                # wrap them in Variable
                if use_gpu:
                    inputs = Variable(inputs.cuda())
                    labels = Variable(labels.cuda())
                else:
                    inputs, labels = Variable(inputs), Variable(labels)

                # zero the parameter gradients
                optimizer.zero_grad()

                # forward
                outputs,outputs2= model(inputs,labels)
                
                

                loss = criterion(outputs, outputs2)

                # backward + optimize only if in training phase
                if phase == 'train':
                    loss.backward()
                    optimizer.step()
                torch.nn.utils.clip_grad_norm(model.parameters(),2.)
               
                

                # statistics
                running_loss += loss.data[0] 
                

            epoch_loss = running_loss / dataset_sizes[phase]
            

            print('{} Loss: {:.4f} nest_loss {}'.format(
                phase, epoch_loss, best_loss))

            # deep copy the model
            if phase == 'val' and epoch_loss < best_loss:
                best_loss = epoch_loss
                best_model_wts = copy.deepcopy(model.state_dict())

      

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(
        time_elapsed // 60, time_elapsed % 60))
    print('Best val Acc: {:4f}'.format(best_loss))

    # load best model weights
    model.load_state_dict(best_model_wts)
    return model




######################################################################
# Finetuning the convnet
# ----------------------
#
# Load a pretrained model and reset final fully connected layer.
#

# model_ft = models.resnet18(pretrained=True)
# num_ftrs = model_ft.fc.in_features
# model_ft.fc = nn.Linear(num_ftrs, 2)

# if use_gpu:
#     model_ft = model_ft.cuda()

# criterion = nn.CrossEntropyLoss()

# # Observe that all parameters are being optimized
# optimizer_ft = optim.SGD(model_ft.parameters(), lr=0.001, momentum=0.9)

# # Decay LR by a factor of 0.1 every 7 epochs
# exp_lr_scheduler = lr_scheduler.StepLR(optimizer_ft, step_size=7, gamma=0.1)

# ######################################################################
# # Train and evaluate
# # ^^^^^^^^^^^^^^^^^^
# #
# # It should take around 15-25 min on CPU. On GPU though, it takes less than a
# # minute.
# #

# model_ft = train_model(model_ft, criterion, optimizer_ft, exp_lr_scheduler,
#                        num_epochs=25)

# ######################################################################
# #

# visualize_model(model_ft)


######################################################################
# ConvNet as fixed feature extractor
# ----------------------------------
#
# Here, we need to freeze all the network except the final layer. We need
# to set ``requires_grad == False`` to freeze the parameters so that the
# gradients are not computed in ``backward()``.
#
# You can read more about this in the documentation
# `here <http://pytorch.org/docs/notes/autograd.html#excluding-subgraphs-from-backward>`__.
#

# model_conv = torchvision.models.resnet18(pretrained=True)


# Parameters of newly constructed modules have requires_grad=True by default
# num_ftrs = model_conv.fc.in_features
# model_conv.avgpool=nn.AvgPool2d(kernel_size=(14,14),stride=1)
# model_conv.fc = nn.Linear(num_ftrs, 256)
# model_conv=nn.Sequential(*list(model_conv.features.children()[-1]))
# model_conv=resnet18(pretrained=True)
model_conv=offshelf()

# print(model_conv.parameters())


if use_gpu:
    model_conv = model_conv.cuda()

criterion = HuberLoss(3.)

# Observe that only parameters of final layer are being optimized as
# opoosed to before.
lists=[i for i in model_conv.fc_a.parameters()]
lists+=[i for i in model_conv.fc_s.parameters()]
# print(lists)
optimizer_conv = optim.SGD(lists, lr=0.0001, momentum=0.9,weight_decay=0.0005)

# Decay LR by a factor of 0.1 every 7 epochs
exp_lr_scheduler = lr_scheduler.StepLR(optimizer_conv, step_size=7, gamma=0.1)


######################################################################
# Train and evaluate
# ^^^^^^^^^^^^^^^^^^
#
# On CPU this will take about half the time compared to previous scenario.
# This is expected as gradients don't need to be computed for most of the
# network. However, forward does need to be computed.
#

model_conv = train_model(model_conv, criterion, optimizer_conv,
                         exp_lr_scheduler, num_epochs=6)
torch.save(model_conv,'model_resnet18_imagenet')

######################################################################
#

# visualize_model(model_conv)

plt.ioff()
plt.show()
