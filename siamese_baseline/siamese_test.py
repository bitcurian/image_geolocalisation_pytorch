


from __future__ import print_function, division
from loss import huber_loss,HuberLoss
import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
from torch.autograd import Variable
import numpy as np
import torchvision
from torch.utils.data import Dataset, DataLoader
from torchvision import datasets, models, transforms
import matplotlib.pyplot as plt
import time
import os
import copy
import re
import pandas as pd
import sys
from skimage import io,transform
from PIL import Image
from resnet_baseline import resnet18
import torch.nn.functional as F
# from transformation_module import transfnet
import math
import argparse
import re
import random
pattern=re.compile('/[0-9\_\.\-]*?(?=_nadir\.png)')
s='aerial/nadir2/40.4438_-79.9981_nadir.png'


batch_size=1
# CREATE_REFERENCE_DATASET=False
reference_dataset=True
path_aerial='./image_retrieval/aerial'
path_street='./image_retrieval/street'
TEST_FILE='../data_nathans_test.csv'
use_gpu=torch.cuda.is_available()
data_transforms = {
    'aerial_image': transforms.Compose([
        transforms.Resize((224,224)),
        transforms.ToTensor(),
        transforms.Normalize([.3802,.3770,.3493], [.1999,.1997,.1928])
    ]),
    'street_image': transforms.Compose([
        transforms.Resize(640,640),
        transforms.CenterCrop(640),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ]),
}


def get_args():
	parser=argparse.ArgumentParser(description='Testing nathans_model')
	
	parser.add_argument('--path_aerial',type=str,default=path_aerial)
	parser.add_argument('--path_street',type=str,default=path_street)
	parser.add_argument('--batch_size',type=int,default=batch_size)
	parser.add_argument('--test_file',type=str,default=TEST_FILE)

	return parser.parse_args()

class AerialDataset(Dataset):
    """Face Landmarks dataset."""

    def __init__(self, csv_file, root_dir, transform=None):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a batch.
        """
        self.data_frame = pd.read_csv(csv_file)
        self.root_dir = root_dir
        self.transform = transform

    def __len__(self):
        return len(self.data_frame)

    def __getitem__(self, idx):
        aerial_img_name = os.path.join(self.root_dir,
                                self.data_frame.iloc[idx, 0])
        aerial_image = io.imread(aerial_img_name)
        aerial_image=Image.fromarray(np.uint8(aerial_image))
        # aerial_image=np.transpose(aerial_image,(2,0,1))
        street_image_name=os.path.join(self.root_dir,self.data_frame.iloc[idx,1])
        street_image=io.imread(street_image_name)
        street_image=Image.fromarray((street_image[:,:,:3]))
# CHANGE street_image to convert RGBA to RGB
        # street_image=np.transpose(street_image,(2,0,1))
        print(aerial_img_name)
        aerial_gps=re.search(pattern,aerial_img_name).group(0)
       	aerial_gps=aerial_gps.replace('/','')
        if self.transform:
        	aerial_image=self.transform[0](aerial_image)
        	street_image=self.transform[1](street_image)
        batch={'aerial_image':aerial_image,'street_image':street_image,'image_gps':aerial_gps}

        return batch

class AerialDataset_Test(Dataset):
    """Face Landmarks dataset."""

    def __init__(self, csv_file, root_dir, transform=None):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a batch.
        """
        self.data_frame = pd.read_csv(csv_file)
        self.root_dir = root_dir
        self.transform = transform

    def __len__(self):
        return len(self.data_frame)

    def __getitem__(self, idx):
        aerial_img_name = os.path.join(self.root_dir,
                                self.data_frame.iloc[idx, 0])
        # print(aerial_img_name)
        
        aerial_image = torch.load(aerial_img_name)

       
        street_image_name=os.path.join(self.root_dir,self.data_frame.iloc[idx,1])
        street_image=torch.load(street_image_name)
        
# CHANGE street_image to convert RGBA to RGB
        # street_image=np.transpose(street_image,(2,0,1))
        # print(aerial_img_name)
        aerial_img_name=aerial_img_name.split('/')
        # print(aerial_img_name[-1])
        aerial_gps=aerial_img_name[-1]
        aerial_gps=aerial_gps.replace('/','')

        batch={'aerial_image':aerial_image.data,'street_image':street_image.data,'image_gps':aerial_gps}
        # sys.exit(0)
        return batch



class siamese_baseline(nn.Module):
    def __init__(self,params=None,num_class=4,source_size=(14,14),target_size=(14,28),batch_size=4):
        super(siamese_baseline,self).__init__()
        self.params=params
        self.source_size=source_size
        self.target_size=target_size
# Set up params to load the resnet18 weights from places dataset       
        self.num_class=num_class
        self.batch_size=batch_size
        # self.pool=nn.AvgPool2d(kernel_size=(2,2),stride=2)
        model_ft=resnet18(pretrained=True,hyper_column=False,no_fc=True)
        for p in model_ft.parameters():
            p.bias=True
                  
        # model_ft.fc=nn.Linear(512,256)
        model_ft2=model_ft
        # model_ft2.fc=nn.Linear(4096,256)
        self.fc_a=nn.Linear(512,256)

        # self.fc_s=nn.Linear(4096,1024)
        self.fc_s2=nn.Linear(512,256)
        
        self.model_ft=model_ft
        self.model_ft2=model_ft2

#hyper input needs to be replaced        
        self.pool1=nn.AvgPool2d(kernel_size=(7,7),stride=1)
        self.pool2=nn.AvgPool2d(kernel_size=(7,14),stride=1)

        # self.conv1=nn.Sequential(nn.Conv2d(960,512,kernel_size=(1,1)),nn.BatchNorm2d(512),nn.PReLU())
        # self.conv2=nn.Sequential(nn.Conv2d(512,512,kernel_size=(1,1)),nn.BatchNorm2d(512),nn.PReLU())
        # self.conv3=nn.Conv2d(512,self.num_class,(1,1))

        # self.conv1st=nn.Sequential(nn.Conv2d(960,512,kernel_size=(1,1)),nn.BatchNorm2d(512),nn.PReLU())
        # self.conv2st=nn.Sequential(nn.Conv2d(512,512,kernel_size=(1,1)),nn.BatchNorm2d(512),nn.PReLU())
        # self.conv3st=nn.Conv2d(512,self.num_class,(1,1))        




    def forward(self,aerial_view,street_view):
        
        print(aerial_view.shape)
        
        # x1,HyperColumn1=self.model_ft(aerial_view)
        # x2,HyperColumn2=self.model_ft2(street_view)
        x1=self.model_ft(aerial_view)
        x2=self.model_ft2(street_view)
        # x1=self.conv1(HyperColumn1)
        # x1=self.conv2(x1)
        # x1=self.pool1(x1)
        
        x1=self.pool1(x1)
        print(x1.shape)
        x1=x1.view((x1.shape[0],-1))
        x1=self.fc_a(x1)

        # x2=self.conv1(HyperColumn2)
        # x2=self.conv2(x2)
        # x2=self.pool2(x2)
        
        x2=self.pool2(x2)
        print(x2.shape)
        x2=x2.view(x2.shape[0],-1)
        # x2=self.fc_s(x2)
        x2=self.fc_s2(x2)
        
        print('aerial view is {} {}'.format(x1.shape,x2.shape))
        
        


        

        print('shapes of the aerial and street view feature maps {} {}'.format(x1.shape,x2.shape))
        return x1,x2       
# class siamese_baseline(nn.Module):
#     def __init__(self,params=None,num_class=4,source_size=(14,14),target_size=(14,28),batch_size=4):
#         super(siamese_baseline,self).__init__()
#         self.params=params
#         self.source_size=source_size
#         self.target_size=target_size
# # Set up params to load the resnet18 weights from places dataset       
#         self.num_class=num_class
#         self.batch_size=batch_size
#         # self.pool=nn.AvgPool2d(kernel_size=(2,2),stride=2)
#         model_ft=resnet18(pretrained=True)
#         for p in model_ft.parameters():
#             p.bias=True
                  
#         # model_ft.fc=nn.Linear(512,256)
#         model_ft2=model_ft
#         # model_ft2.fc=nn.Linear(4096,256)
#         self.fc_a=nn.Linear(512,256)

#         self.fc_s=nn.Linear(4096,1024)
#         self.fc_s2=nn.Linear(1024,256)
        
#         self.model_ft=model_ft
#         self.model_ft2=model_ft2

# #hyper input needs to be replaced        
        
#         # self.conv1=nn.Sequential(nn.Conv2d(960,512,kernel_size=(1,1)),nn.BatchNorm2d(512),nn.PReLU())
#         # self.conv2=nn.Sequential(nn.Conv2d(512,512,kernel_size=(1,1)),nn.BatchNorm2d(512),nn.PReLU())
#         # self.conv3=nn.Conv2d(512,self.num_class,(1,1))

#         # self.conv1st=nn.Sequential(nn.Conv2d(960,512,kernel_size=(1,1)),nn.BatchNorm2d(512),nn.PReLU())
#         # self.conv2st=nn.Sequential(nn.Conv2d(512,512,kernel_size=(1,1)),nn.BatchNorm2d(512),nn.PReLU())
#         # self.conv3st=nn.Conv2d(512,self.num_class,(1,1))        




#     def forward(self,aerial_view,street_view):
        
#         print(aerial_view.shape)
        
#         x1=self.model_ft(aerial_view)
#         x2=self.model_ft2(street_view)
#         x1=self.fc_a(x1)
#         x2=self.fc_s(x2)
#         x2=self.fc_s2(x2)
        
#         print('aerial view is {} {}'.format(x1.shape,x2.shape))
        


        

#         print('shapes of the aerial and street view feature maps {} {}'.format(x1.shape,x2.shape))
#         return x1,x2
# class siamese_baseline(nn.Module):
#     def __init__(self,params=None,num_class=4,source_size=(14,14),target_size=(14,28),batch_size=4):
#         super(siamese_baseline,self).__init__()
#         self.params=params
#         self.source_size=source_size
#         self.target_size=target_size
# # Set up params to load the resnet18 weights from places dataset       
#         self.num_class=num_class
#         self.batch_size=batch_size

#         model_ft=resnet18(pretrained=True)
#         model_ft.fc=nn.Linear(512,1024)
#         model_ft2=model_ft
        
        
#         self.model_ft=model_ft
#         self.model_ft2=model_ft2

# #hyper input needs to be replaced        
        
#         self.conv1=nn.Sequential(nn.Conv2d(960,512,kernel_size=(1,1)),nn.BatchNorm2d(512),nn.ReLU())
#         self.conv2=nn.Sequential(nn.Conv2d(512,512,kernel_size=(1,1)),nn.BatchNorm2d(512),nn.ReLU())
#         self.conv3=nn.Conv2d(512,self.num_class,(1,1))

#         self.conv1st=nn.Sequential(nn.Conv2d(960,512,kernel_size=(1,1)),nn.BatchNorm2d(512),nn.ReLU())
#         self.conv2st=nn.Sequential(nn.Conv2d(512,512,kernel_size=(1,1)),nn.BatchNorm2d(512),nn.ReLU())
#         self.conv3st=nn.Conv2d(512,self.num_class,(1,1))        




#     def forward(self,aerial_view,street_view):
        
#         print(aerial_view.shape)
        
#         x1=self.model_ft(aerial_view)
#         x2=self.model_ft2(street_view)
        
#         print('aerial view is {}'.format(x1.shape))


        

        # print('shapes of the aerial and street view feature maps {} {}'.format(x1.shape,x2.shape))
        # return x1,x2
class offshelf(nn.Module):

    def __init__(self):
        super(offshelf,self).__init__()

        model=resnet18(pretrained=True)
        for param in model.parameters():
            param.requires_grad = False      
        self.model=model  
        self.pool_a=nn.AvgPool2d(kernel_size=(7,7),stride=1)
        self.pool_s=nn.AvgPool2d(kernel_size=(7,14),stride=1)

        self.fc_a=nn.Linear(512,256)
        self.fc_s=nn.Linear(512,256)

    def forward(self,aerial,street):
        x1=self.model(aerial)
        x1=self.pool_a(x1)
        x1=x1.view(x1.shape[0],-1)
        x1=self.fc_a(x1)

        x2=self.model(street)
        x2=self.pool_s(x2)
        x2=x2.view(x1.shape[0],-1)
        x2=self.fc_s(x2)

        return x1,x2

def create_reference_dataset(args):
	data_test_actual=AerialDataset('../aerial/data_op_test.csv','../aerial',transform=[data_transforms['aerial_image'],transforms.Compose([transforms.Resize((224,448)),transforms.ToTensor()]),transforms.Normalize([.4881,.5005,.5113], [.1888,.1843,.1968])])
	dataLoader_actual_test=torch.utils.data.DataLoader(data_test_actual,batch_size=args.batch_size,shuffle=True,num_workers=4)
	model=torch.load('./model_ft_30PReLU30.pth')
	# model.__init__(batch_size=args.batch_size)
	model=model.cuda()
	model.eval()
	f=open(args.test_file,'w+')
	running_loss=0
	for i,data in enumerate(dataLoader_actual_test):
		inputs,targets,label_gps=data['aerial_image'],data['street_image'],data['image_gps']
		if(use_gpu==True):
		    inputs=Variable(inputs.cuda())
		    targets=Variable(targets.cuda())
		outputs1,outputs2=model(inputs,targets)



		print(outputs1,outputs2)


		loss=Variable(torch.FloatTensor(1,args.batch_size).zero_())

		loss=F.pairwise_distance(outputs1,outputs2)
		print(loss.data)
		for i in range(0,args.batch_size):
			torch.save(outputs1[i],path_aerial+os.sep+str(label_gps[i]))
			torch.save(outputs2[i],path_street+os.sep+str(label_gps[i]))



			f.write(path_aerial+os.sep+label_gps[i]+','+path_street+os.sep+label_gps[i]+'\n')

		iteration_loss=loss.data[0]*args.batch_size

		print(iteration_loss.shape)

		running_loss+=iteration_loss



		print(running_loss/len(data_test_actual))
	f.close()

def pairwise_distances(x, y=None):

    x_norm = (x**2).sum(1).view(-1, 1)
    if y is not None:
        y_t = torch.transpose(y, 0, 1)
        y_norm = (y**2).sum(1).view(1, -1)
    else:
        y_t = torch.transpose(x, 0, 1)
        y_norm = x_norm.view(1, -1)
    
    dist = x_norm + y_norm - 2.0 * torch.mm(x, y_t)

    return torch.clamp(dist, 0.0, np.inf)

def dist_gps_coordinates(coord1,coord2):

    lat_p=re.compile('.*?(?=_)')
    lon_pattern=re.compile('_([0-9\-\.]*)')
    lat1=float(re.search(lat_p,coord1).group())
    lon1=float(re.search(lon_pattern,coord1).group(1))
    lat2=float(re.search(lat_p,coord2).group())
    lon2=float(re.search(lon_pattern,coord2).group(1))
    print(lat1,lon1)
    print(lat2,lon2)

    d=2*math.asin(math.sqrt((math.sin((lat1-lat2)/2))**2 + 
                 math.cos(lat1)*math.cos(lat2)*(math.sin((lon1-lon2)/2))**2))
    return d*10**5

def test(args,random_method=False):


    flag=1
    data_test_actual=AerialDataset_Test(args.test_file,'')
    f1=open('siamese30PReLU30_baseline_scores.csv','w+')
    dataLoader_actual_test=torch.utils.data.DataLoader(data_test_actual,batch_size=args.batch_size,shuffle=False)
    aerial_list=[]
    street_list=[]
    gps_list=[]
	# correct=0
    correct=0
    for data in dataLoader_actual_test:
        aerial,street,im_gps=data['aerial_image'],data['street_image'],data['image_gps']
        
        for i in range(0,aerial.shape[0]):
            aerial_list.append(aerial[i])
            street_list.append(street[i])
            gps_list.append(im_gps[i])
            if(flag==1):
                print('aerial 0 before list {}'.format(aerial[0]))
                flag=0
    aerial_matrix=torch.stack(aerial_list,dim=0)
    street_matrix=torch.stack(street_list,dim=0)


    torch.set_printoptions(threshold=5000)

    distance_matrix=pairwise_distances(street_matrix,aerial_matrix)
    if(random_method==True):
        idx=[[],[]]
        for i in range(0,len(data_test_actual)):
            r=random.randint(0,len(data_test_actual)-1)
            idx[1].append(r)
    else:
        idx=distance_matrix.min(dim=1)

    list_dist=[0,50,100,150,200,250,300]


    for k in list_dist:
        correct=0
        for i in range(0,len(idx[1])):
            print(idx[1][i],i)
            print('distance is {}'.format(idx[0][i]))

            street=i
            aerial=idx[1][i]
            if(dist_gps_coordinates(gps_list[street],gps_list[aerial])<=k):
                print(i,idx[1][i])
                print(gps_list[street],gps_list[aerial])
                print('\n')

                correct+=1

        print('correct ones {}'.format(correct))
        print('total number {}'.format(len(idx[1])))
        print('top-1 accuracy for range-{} :{}'.format(k,correct/len(idx[1])))
        f1.write(str(k)+','+str(correct/len(idx[1]))+'\n')
		





def main():
	args=get_args()
	print(args,reference_dataset)
	if(reference_dataset==True):
		create_reference_dataset(args)
	test(args,random_method=False)

if __name__ == '__main__':
	main()


