

from __future__ import print_function, division
from loss import huber_loss,HuberLoss
import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
from torch.autograd import Variable
import numpy as np
import torchvision
from torch.utils.data import Dataset, DataLoader
from torchvision import datasets, models, transforms
import matplotlib.pyplot as plt
import time
import os
import copy
import re
import pandas as pd
import sys
from skimage import io,transform
from PIL import Image
from resnet import resnet18
import torch.nn.functional as F
from transformation_module import transfnet

import argparse
import re

pattern=re.compile('/[0-9\_\.\-]*?(?=_nadir\.png)')
s='aerial/nadir2/40.4438_-79.9981_nadir.png'


batch_size=1
# CREATE_REFERENCE_DATASET=False
reference_dataset=True
path_aerial='./image_retrieval/nathans_model/aerial'
path_street='./image_retrieval/nathans_model/street'
TEST_FILE='data_nathans_test.csv'
use_gpu=torch.cuda.is_available()
data_transforms = {
    'aerial_image': transforms.Compose([
        transforms.Resize((224,224)),
        transforms.ToTensor(),
        transforms.Normalize([.3802,.3770,.3493], [.1999,.1997,.1928])
    ]),
    'street_image': transforms.Compose([
        transforms.Resize(640,640),
        transforms.CenterCrop(640),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ]),
}


def get_args():
	parser=argparse.ArgumentParser(description='Testing nathans_model')
	
	parser.add_argument('--path_aerial',type=str,default=path_aerial)
	parser.add_argument('--path_street',type=str,default=path_street)
	parser.add_argument('--batch_size',type=int,default=batch_size)
	parser.add_argument('--test_file',type=str,default=TEST_FILE)

	return parser.parse_args()

class AerialDataset(Dataset):
    """Face Landmarks dataset."""

    def __init__(self, csv_file, root_dir, transform=None):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a batch.
        """
        self.data_frame = pd.read_csv(csv_file)
        self.root_dir = root_dir
        self.transform = transform

    def __len__(self):
        return len(self.data_frame)

    def __getitem__(self, idx):
        aerial_img_name = os.path.join(self.root_dir,
                                self.data_frame.iloc[idx, 0])
        aerial_image = io.imread(aerial_img_name)
        aerial_image=Image.fromarray(np.uint8(aerial_image))
        # aerial_image=np.transpose(aerial_image,(2,0,1))
        street_image_name=os.path.join(self.root_dir,self.data_frame.iloc[idx,1])
        street_image=io.imread(street_image_name)
        street_image=Image.fromarray((street_image[:,:,:3]))
# CHANGE street_image to convert RGBA to RGB
        # street_image=np.transpose(street_image,(2,0,1))
        print(aerial_img_name)
        aerial_gps=re.search(pattern,aerial_img_name).group(0)
       	aerial_gps=aerial_gps.replace('/','')
        if self.transform:
        	aerial_image=self.transform[0](aerial_image)
        	street_image=self.transform[1](street_image)
        batch={'aerial_image':aerial_image,'street_image':street_image,'image_gps':aerial_gps}

        return batch

class AerialDataset_Test(Dataset):
    """Face Landmarks dataset."""

    def __init__(self, csv_file, root_dir, transform=None):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a batch.
        """
        self.data_frame = pd.read_csv(csv_file)
        self.root_dir = root_dir
        self.transform = transform

    def __len__(self):
        return len(self.data_frame)

    def __getitem__(self, idx):
        aerial_img_name = os.path.join(self.root_dir,
                                self.data_frame.iloc[idx, 0])
        # print(aerial_img_name)
        
        aerial_image = torch.load(aerial_img_name)

       
        street_image_name=os.path.join(self.root_dir,self.data_frame.iloc[idx,1])
        street_image=torch.load(street_image_name)
        
# CHANGE street_image to convert RGBA to RGB
        # street_image=np.transpose(street_image,(2,0,1))
        # print(aerial_img_name)
        aerial_img_name=aerial_img_name.split('/')
        # print(aerial_img_name[-1])
        aerial_gps=aerial_img_name[-1]
        aerial_gps=aerial_gps.replace('/','')

        batch={'aerial_image':aerial_image.data,'street_image':street_image.data,'image_gps':aerial_gps}
        # sys.exit(0)
        return batch

class nathans_model(nn.Module):
    def __init__(self,params=None,num_class=4,source_size=(14,14),target_size=(14,28),batch_size=4):
        super(nathans_model,self).__init__()
        self.params=params
        self.source_size=source_size
        self.target_size=target_size
# Set up params to load the resnet18 weights from places dataset       
        self.num_class=num_class
        self.batch_size=batch_size

        model_ft=resnet18(pretrained=True,no_fc=True)
        model_ft2=resnet18(pretrained=True,no_fc=True,street_view=True)
        self.model_ft=model_ft
        self.model_ft2=model_ft2
        T=transfnet(self.source_size,self.target_size,self.batch_size)
        self.T=T
#hyper input needs to be replaced        
        hyper_input=512
        self.conv1=nn.Sequential(nn.Conv2d(hyper_input,512,kernel_size=(1,1)),nn.BatchNorm2d(512),nn.ReLU())
        self.conv2=nn.Sequential(nn.Conv2d(512,512,kernel_size=(1,1)),nn.BatchNorm2d(512),nn.ReLU())
        self.conv3=nn.Conv2d(512,self.num_class,(1,1))

        self.conv1st=nn.Sequential(nn.Conv2d(hyper_input,512,kernel_size=(1,1)),nn.BatchNorm2d(512),nn.ReLU())
        self.conv2st=nn.Sequential(nn.Conv2d(512,512,kernel_size=(1,1)),nn.BatchNorm2d(512),nn.ReLU())
        self.conv3st=nn.Conv2d(512,self.num_class,(1,1))        

    def forward(self,aerial_view,street_view):
        
        print(aerial_view.shape)
        
        x1=self.model_ft(aerial_view)
        print('aerial view is {}'.format(x1.shape))
        # sys.exit(0)

#Gets the last set of conv. feature maps as input        
        self.T.compute_transfnet(x1)
# gets an aerial vector as input    
        # h1=model_ft.hyper_column()

        h1=x1
        
        h1=self.conv1(h1)
        h1=self.conv2(h1)
        h1=self.conv3(h1)



#Aerial vector is h1 as HxWX1

        x1=self.T.transfnet(h1)
        x1=torch.sum(x1,dim=1)
#Summing the feature maps before los calculation
        x2=self.model_ft2(street_view)
        print(x2.shape)
        x2=self.conv1st(x2)
        x2=self.conv2st(x2)
        x2=self.conv3st(x2)
        x2=torch.sum(x2,dim=1)

        print('street_view is {}'.format(x2.shape))        

        print('shapes of the aerial and street view feature maps {} {}'.format(x1.shape,x2.shape))
        return x1,x2

def create_reference_dataset(args):
	data_test_actual=AerialDataset('./aerial/data_op_test.csv','aerial',transform=[data_transforms['aerial_image'],transforms.Compose([transforms.Resize((224,448)),transforms.ToTensor()]),transforms.Normalize([.4881,.5005,.5113], [.1888,.1843,.1968])])
	dataLoader_actual_test=torch.utils.data.DataLoader(data_test_actual,batch_size=args.batch_size,shuffle=True,num_workers=4)
	model=torch.load('./model_ft_imagenet.pth')
	model.__init__(batch_size=args.batch_size)
	model=model.cuda()
	model.eval()
	f=open(args.test_file,'w+')
	running_loss=0
	for i,data in enumerate(dataLoader_actual_test):
		inputs,targets,label_gps=data['aerial_image'],data['street_image'],data['image_gps']
		if(use_gpu==True):
		    inputs=Variable(inputs.cuda())
		    targets=Variable(targets.cuda())
		outputs1,outputs2=model(inputs,targets)

		#REPLACE huber_loss with l2 distance   

		# outputs1=outputs1.unsqueeze(dim=1)
		# outputs2=outputs2.unsqueeze(dim=1)

		# print(outputs1.shape,outputs2.shape)

		outputs1=outputs1.view((args.batch_size,outputs1.shape[1]*outputs1.shape[2]) )
		outputs2=outputs2.view((args.batch_size,outputs2.shape[1]*outputs2.shape[2]))
		# outputs1=torch.unsqueeze(outputs1,dim=1)
		# outputs2=torch.unsqueeze(outputs2,dim=1)
		loss=Variable(torch.FloatTensor(1,args.batch_size).zero_())

		# for i in range(0,batch_size):
		#     print(outputs1[i].shape,outputs2[i].shape)
		#     loss[i]=F.pairwise_distance(outputs1[i],outputs2[i])
		# print(outputs1.shape,outputs2.shape)
		loss=F.pairwise_distance(outputs1,outputs2)
		print(loss.data)
		for i in range(0,args.batch_size):
			torch.save(outputs1[i],path_aerial+os.sep+str(label_gps[i]))
			torch.save(outputs2[i],path_street+os.sep+str(label_gps[i]))



			f.write(path_aerial+os.sep+label_gps[i]+','+path_street+os.sep+label_gps[i]+'\n')

		iteration_loss=loss.data[0]*args.batch_size

		print(iteration_loss.shape)

		running_loss+=iteration_loss

		# print('loss: {:.4f}'.format(iteration_loss))

		# print(i)


		print(running_loss/len(data_test_actual))
	f.close()

def pairwise_distances(x, y=None):
    '''
    Input: x is a Nxd matrix
           y is an optional Mxd matirx
    Output: dist is a NxM matrix where dist[i,j] is the square norm between x[i,:] and y[j,:]
            if y is not given then use 'y=x'.
    i.e. dist[i,j] = ||x[i,:]-y[j,:]||^2
    '''
    x_norm = (x**2).sum(1).view(-1, 1)
    if y is not None:
        y_t = torch.transpose(y, 0, 1)
        y_norm = (y**2).sum(1).view(1, -1)
    else:
        y_t = torch.transpose(x, 0, 1)
        y_norm = x_norm.view(1, -1)
    
    dist = x_norm + y_norm - 2.0 * torch.mm(x, y_t)
    # Ensure diagonal is zero if x=y
    # if y is None:
    #     dist = dist - torch.diag(dist.diag)
    return torch.clamp(dist, 0.0, np.inf)


def test(args):

	
	data_test_actual=AerialDataset_Test(args.test_file,'')
	dataLoader_actual_test=torch.utils.data.DataLoader(data_test_actual,batch_size=args.batch_size,shuffle=False)
	aerial_list=[]
	street_list=[]
#Improve directory creation later	
	# folders=args.path_aerial.split('/')
	# for i in folders:_list
	# 	if()

	for data in dataLoader_actual_test:
		aerial,street,im_gps=data['aerial_image'],data['street_image'],data['image_gps']
		print(aerial[0].shape)
		for i in range(0,aerial.shape[0]):
			aerial_list.append(aerial[i])
			street_list.append(street[i])
	aerial_matrix=torch.stack(aerial_list,dim=0)
	street_matrix=torch.stack(street_list,dim=0)
	# print(aerial_matrix.shape,street_matrix.shape)
	torch.set_printoptions(threshold=5000)

	distance_matrix=pairwise_distances(street_matrix,aerial_matrix)
	# distance_matrix=torch.nn.functional.softmax(Variable(distance_matrix),dim=1)
	# print(distance_matrix[0:3,:],distance_matrix.shape)
	# print(distance_matrix.min(dim=1))
	correct=0
	idx=distance_matrix.min(dim=1)
	print(idx[1][:],type(idx),len(idx[1]))
	
	for i in range(0,len(idx[1])):
		print(idx[1][i],i)
		# a=input(('cont?'))
		if(idx[1][i]==i):
			print(i,idx[1][i])
			correct+=1

	print('correct ones {}'.format(correct))
	print('total number {}'.format(len(idx[1])))
	print('top-1 accuracy {}'.format(correct/len(idx[1])))


		





def main():
	args=get_args()
	print(args,reference_dataset)
	if(reference_dataset==True):
		create_reference_dataset(args)
	test(args)

if __name__ == '__main__':
	main()


