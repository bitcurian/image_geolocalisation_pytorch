import torch
from torch.autograd import Variable
import numpy as np
import torch.nn as nn
import sys
import torch.nn.functional as F
def huber_loss(source, target,threshold=1.0):
	error=torch.abs(source-target)
	linear=threshold*(error-.5*threshold)
	quadratic=.5*error*error

	print(source.shape,target.shape)
	# sys.exit(0)
	return torch.sum(where(error<threshold,quadratic,linear))

def where(cond, x_1, x_2):

    cond = cond.float()    
    
    # print(cond.is_cuda,x_1.is_cuda,x_2.is_cuda)
    # sys.exit(0)
    return (cond * x_1) + ((1-cond) * x_2)


class HuberLoss(nn.Module):

    def __init__(self, margin=1.0):
        super(HuberLoss, self).__init__()
        self.margin = margin

    def forward(self, source,target):
        # euclidean_distance = F.pairwise_distance(source,target)
        # loss_contrastive = torch.mean((1-label) * torch.pow(euclidean_distance, 2) +
        #                               (label) * torch.pow(torch.clamp(self.margin - euclidean_distance, min=0.0), 2))
        error=torch.abs(source-target)
        quad=0.5*error*error
        linear=self.margin*(error-.5*self.margin)

        return torch.sum(where(error<self.margin,quad,quad))