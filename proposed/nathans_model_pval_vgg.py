

from __future__ import print_function, division
from loss import huber_loss,HuberLoss
import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
from torch.autograd import Variable
import numpy as np
import torchvision
from torch.utils.data import Dataset, DataLoader
from torchvision import datasets, models, transforms
import matplotlib.pyplot as plt
import time
import os
import copy
import re
import pandas as pd
import sys
from skimage import io,transform
from PIL import Image
# from resnet import resnet18
import torch.nn.functional as F
from tensorboardX import SummaryWriter
from transformation_module import transfnet
from torchviz import make_dot
from vgg_nathans import vgg16_bn
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="1,0"
plt.ion()   # interactive mode
batch_size=3
writer=SummaryWriter()

# test_lastconv=Variable(torch.rand((1,128,4,4)))
# test_lastconv=test_lastconv.cuda()
# aerial_vector=Variable(torch.rand((1,4,4,4)))
# # aerial_vector=aerial_vector.cuda()

# transformation=transfnet(source_size=(4,4),target_size=(2,6),batch_size=batch_size,net=test_lastconv)
# transformation=transformation.cuda()
# transformation.compute_transfnet()
# street_view=transformation.transfnet(aerial_vector)
# print('street view shape is {}'.format(street_view.shape))
# 

# model_ft=resnet18(pretrained=True,no_fc=True)
# model_ft2=resnet18(pretrained=True,no_fc=True,street_view=True)



# if use_gpu:
#     model_ft = model_ft.cuda()
#     model_ft2=model_ft2.cuda()

class nathans_model(nn.Module):
    def __init__(self,params=None,num_class=4,source_size=(17,17),target_size=(17,45),batch_size=4):
        super(nathans_model,self).__init__()
        self.params=params
        self.source_size=source_size
        self.target_size=target_size
# Set up params to load the resnet18 weights from places dataset       
        self.num_class=num_class
        self.batch_size=batch_size
        # self.pool=nn.AvgPool2d(kernel_size=(2,2),stride=2)
        # model_ft=resnet18(pretrained=True,hyper_column=False,no_fc=True)
        T=transfnet(self.source_size,self.target_size,self.batch_size)
        self.T=T
        model_ft=vgg16_bn(pretrained=True)
        model_ft2=vgg16_bn(pretrained=True)
        self.model_ft=nn.Sequential(*list(model_ft.features.children())[:-12])
        self.model_ft2=nn.Sequential(*list(model_ft2.features.children())[:-12])

        for p1,p2 in zip(self.model_ft,self.model_ft2):
            p1.requires_grad=False
            p2.requires_grad=False
        self.pool22=nn.AvgPool2d(kernel_size=(15,15),stride=2)
        self.pool12=nn.AvgPool2d(kernel_size=(5,5),stride=6)
        self.pool5=nn.AvgPool2d(kernel_size=(17,17),stride=12)

        self.pool22s=nn.AvgPool2d(kernel_size=(15,15),stride=(2,2))
        self.pool12s=nn.AvgPool2d(kernel_size=(5,42),stride=(6,4))
        self.pool5s=nn.AvgPool2d(kernel_size=(17,4),stride=(12,10))

        # for p,p2 in zip(model_ft.parameters(),model_ft2.parameters()):
        #     p.bias=True
        #     p2.bias=True
    

# #hyper input needs to be replaced        

        self.conv1=nn.Sequential(nn.Conv2d(960,512,kernel_size=(1,1)),nn.BatchNorm2d(512),nn.ReLU())
        self.conv2=nn.Sequential(nn.Conv2d(512,512,kernel_size=(1,1)),nn.BatchNorm2d(512),nn.ReLU())
        self.conv3=nn.Conv2d(512,self.num_class,(1,1))

        self.conv1st=nn.Sequential(nn.Conv2d(960,512,kernel_size=(1,1)),nn.BatchNorm2d(512),nn.ReLU())
        self.conv2st=nn.Sequential(nn.Conv2d(512,512,kernel_size=(1,1)),nn.BatchNorm2d(512),nn.ReLU())
        self.conv3st=nn.Conv2d(512,self.num_class,(1,1))        




    def forward(self,aerial_view,street_view):
        
        # print(aerial_view.shape)
        
        
        # x1,HyperColumn1=self.model_ft(aerial_view)
        # x2,HyperColumn2=self.model_ft2(street_view)
        x1=aerial_view
        x2=street_view
        module_list=list(self.model_ft)
        module_list2=list(self.model_ft2)
        for i in range(len(module_list)):
            # print(i)
            x1=module_list[i](x1)
            x2=module_list2[i](x2)
            # print(x1.shape)
            if(i==5):
                
                # h1=x1
                h1=self.pool5(x1)
                h1s=self.pool5s(x2)
                # print(x1.shape)
                # print('\n {} new {}'.format(x2.shape,h1s.shape))
            if(i==12):
                # print(x1.shape)

                # h2=x1
                h2=self.pool12(x1)
                h2s=self.pool12s(x2)
                # print(x1.shape)
                print('\n {} new {}'.format(x2.shape,h2s.shape))
            if(i==22):
                # print(x1.shape)
                h3=self.pool22(x1)
                h3s=self.pool22s(x2)
                # print(x1.shape)
                print('\n {} new {}'.format(x2.shape,h3s.shape))



        

        # x2=self.model_ft2(street_view)
        
        # x1=self.pool1(x1)
        
        # x1=self.pool1(x1)
        # print(x1.shape,x2.shape)
        # print(str(self.model_ft))
        HyperColumn1=torch.cat([h1,h2,h3,x1],dim=1)
        HyperColumn2=torch.cat([h1s,h2s,h3s,x2],dim=1)

        self.T.compute_transfnet(x1)
        x1=self.conv1(HyperColumn1)
        x1=self.conv2(x1)
        x1=self.conv3(x1)
        x1=self.T.transfnet(x1)
        x1=torch.sum(x1,dim=1)


        x2=self.conv1st(HyperColumn2)
        x2=self.conv2st(x2)
        x2=self.conv3st(x2)
        x2=torch.sum(x2,dim=1)
        print('aerial view is {} {}'.format(x1.shape,x2.shape))        
        

        # x1=x1.view((x1.shape[0],-1))
        # x1=self.fc_a(x1)

        # x2=self.conv1(HyperColumn2)
        # x2=self.conv2(x2)
        # x2=self.pool2(x2)
        
        # x2=self.pool2(x2)
        # print(x2.shape)
        # x2=x2.view(x2.shape[0],-1)
        # # x2=self.fc_s(x2)
        # x2=self.fc_s2(x2)
        
        print('aerial view is {} {}'.format(x1.shape,x2.shape))
        
        


        

        print('shapes of the aerial and street view feature maps {} {}'.format(x1.shape,x2.shape))
        return x1,x2


# class nathans_model(nn.Module):
#     def __init__(self,params=None,num_class=4,source_size=(14,14),target_size=(14,28),batch_size=4):
#         super(nathans_model,self).__init__()
#         self.params=params
#         self.source_size=source_size
#         self.target_size=target_size
# # Set up params to load the resnet18 weights from places dataset       
#         self.num_class=num_class
#         self.batch_size=batch_size

#         model_ft=resnet18(pretrained=True,no_fc=True,hyper_column=True)
#         model_ft2=resnet18(pretrained=True,no_fc=True,street_view=True,hyper_column=True)
#         for p,p1 in zip(model_ft.parameters(),model_ft2.parameters()):
#             p.requires_grad=False
#             p1.requires_grad=False
#         self.block1=model_ft.layer1
#         self.block2=model_ft.layer2
#         self.block3=model_ft.layer3
#         self.block4=model_ft.layer4


        
#         self.model_ft=model_ft
#         self.model_ft2=model_ft2
#         T=transfnet(self.source_size,self.target_size,self.batch_size)
#         self.T=T
# #hyper input needs to be replaced        
        
#         self.conv1=nn.Sequential(nn.Conv2d(960,512,kernel_size=(1,1)),nn.BatchNorm2d(512),nn.ReLU())
#         self.conv2=nn.Sequential(nn.Conv2d(512,512,kernel_size=(1,1)),nn.BatchNorm2d(512),nn.ReLU())
#         self.conv3=nn.Conv2d(512,self.num_class,(1,1))

#         self.conv1st=nn.Sequential(nn.Conv2d(960,512,kernel_size=(1,1)),nn.BatchNorm2d(512),nn.ReLU())
#         self.conv2st=nn.Sequential(nn.Conv2d(512,512,kernel_size=(1,1)),nn.BatchNorm2d(512),nn.ReLU())
#         self.conv3st=nn.Conv2d(512,self.num_class,(1,1))        




#     def forward(self,aerial_view,street_view):
        
#         print(aerial_view.shape)
        
#         x1,HyperColumn=self.model_ft(aerial_view)
#         print('Hypercolumn is  {}'.format(HyperColumn.shape))

        
#         print('aerial view is {}'.format(x1.shape))
#         # 

# #Gets the last set of conv. feature maps as input        
#         self.T.compute_transfnet(x1)
# # gets an aerial vector as input    
#         # h1=model_ft.hyper_column()

#         # h1=x1
        
#         h1=self.conv1(HyperColumn)
#         h1=self.conv2(h1)
#         h1=self.conv3(h1)



# #Aerial vector is h1 as HxWX1

#         x1=self.T.transfnet(h1)
#         x1=torch.sum(x1,dim=1)
# #Summing the feature maps before loss calculation

#         x2,HyperColumn2=self.model_ft2(street_view)
#         print(x2.shape,HyperColumn2.shape)
#         print(x1.shape,HyperColumn.shape)
        
#         x2=self.conv1st(HyperColumn2)
#         x2=self.conv2st(x2)
#         x2=self.conv3st(x2)
#         x2=torch.sum(x2,dim=1)

#         print('street_view is {}'.format(x2.shape))        

#         print('shapes of the aerial and street view feature maps {} {}'.format(x1.shape,x2.shape))
#         return x1,x2


# def define_model(params):
#     def conv2d(input, params, base, stride=1, pad=0):
#         return F.conv2d(input, params[base + '.weight'],
#                         params[base + '.bias'], stride, pad)

#     def group(input, params, base, stride, n):
#         o = input
#         for i in range(0,n):
#             b_base = ('%s.block%d.conv') % (base, i)
#             x = o
#             o = conv2d(x, params, b_base + '0', pad=1, stride=i==0 and stride or 1)
#             o = F.relu(o)
#             o = conv2d(o, params, b_base + '1', pad=1)
#             if i == 0 and stride != 1:
#                 o += F.conv2d(x, params[b_base + '_dim.weight'], stride=stride)
#             else:
#                 o += x
#             o = F.relu(o)
#         return o
    
#     # determine network size by parameters
#     blocks = [sum([re.match('group%d.block\d+.conv0.weight'%j, k) is not None
#                    for k in params.keys()]) for j in range(4)]

#     def f(input, params):
#         o = F.conv2d(input, params['conv0.weight'], params['conv0.bias'], 2, 3)
#         o = F.relu(o)
#         o = F.max_pool2d(o, 3, 2, 1)
#         o_g0 = group(o, params, 'group0', 1, blocks[0])
#         o_g1 = group(o_g0, params, 'group1', 2, blocks[1])
#         o_g2 = group(o_g1, params, 'group2', 2, blocks[2])
#         o_g3 = group(o_g2, params, 'group3', 2, blocks[3])
#         o = F.avg_pool2d(o_g3, 7, 1, 0)
#         # o = o.view(o.size(0), -1)
#         # o = F.linear(o, params['fc.weight'], params['fc.bias'])
#         return o
#     return f



def calc_mean_std(dataset):

    r_a,r_s=0.0,0.0
    g_a,g_s=0.0,0.0
    b_a,b_s=0.0,0.0
    dataLoader=torch.utils.data.DataLoader(data_train,batch_size=1,num_workers=4)
    for i,data in enumerate(dataLoader):
        inputs1,inputs2=data['aerial_image'],data['street_image']
        inputs1=Variable(inputs1).cuda()
        inputs2=Variable(inputs2).cuda()
        
        r_a+=torch.sum(inputs1[0,0,:,:])
        g_a+=torch.sum(inputs1[0,1,:,:])
        b_a+=torch.sum(inputs1[0,2,:,:])

        r_s+=torch.sum(inputs2[0,0,:,:])
        g_s+=torch.sum(inputs2[0,1,:,:])
        b_s+=torch.sum(inputs2[0,2,:,:])


    aerial,street=data_train[0]['aerial_image'],data_train[0]['street_image']
    print(aerial.shape)
    H_a,W_a=aerial.shape[1:]
    H_s,W_s=street.shape[1:]
 
    mean_ra=r_a/(len(dataset)*H_a*W_a)
    mean_ga=g_a/(len(dataset)*H_a*W_a)
    mean_ba=b_a/(len(dataset)*H_a*W_a)

    mean_rs=r_s/(len(dataset)*H_s*W_s)
    mean_gs=g_s/(len(dataset)*H_s*W_s)
    mean_bs=b_s/(len(dataset)*H_s*W_s)

    std_ra,std_ga,std_ba=0.0,0.0,0.0
    std_rs,std_gs,std_bs=0.0,0.0,0.0
    for i,data in enumerate(dataLoader):
        inputs1,inputs2=data['aerial_image'],data['street_image']
        inputs1=Variable(inputs1).cuda()
        inputs2=Variable(inputs2).cuda()
        std_ra+=torch.sum((inputs1[0,0,:,:]-mean_ra)**2)
        std_ga+=torch.sum((inputs1[0,1,:,:]-mean_ga)**2)
        std_ba+=torch.sum((inputs1[0,2,:,:]-mean_ba)**2)


        std_rs+=torch.sum((inputs2[0,0,:,:]-mean_rs)**2)
        std_gs+=torch.sum((inputs2[0,1,:,:]-mean_gs)**2)
        std_bs+=torch.sum((inputs2[0,2,:,:]-mean_bs)**2)


    std_ra,std_ga,std_ba=torch.sqrt((std_ra)/((H_a*W_a*len(dataset))-1)),torch.sqrt((std_ga)/((H_a*W_a*len(dataset))-1)),torch.sqrt((std_ba)/((H_a*W_a*len(dataset))-1))
    std_rs,std_gs,std_bs=torch.sqrt((std_rs)/((H_s*W_s*len(dataset))-1)),torch.sqrt((std_gs)/((H_s*W_s*len(dataset))-1)),torch.sqrt((std_bs)/((H_s*W_s*len(dataset))-1))

    mean_a=[mean_ra,mean_ga,mean_ba]

    mean_s=[mean_rs,mean_gs,mean_bs]

    std_a=[std_ra,std_ga,std_ba]
    std_s=[std_rs,std_gs,std_bs]

    return mean_a,mean_s,std_a,std_s
        



class AerialDataset(Dataset):
    """Face Landmarks dataset."""

    def __init__(self, csv_file, root_dir, transform=None):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a batch.
        """
        self.data_frame = pd.read_csv(csv_file)
        self.root_dir = root_dir
        self.transform = transform

    def __len__(self):
        return len(self.data_frame)

    def __getitem__(self, idx):
        aerial_img_name = os.path.join(self.root_dir,
                                self.data_frame.iloc[idx, 0])
        aerial_image = io.imread(aerial_img_name)
        aerial_image=Image.fromarray(np.uint8(aerial_image))
        # aerial_image=np.transpose(aerial_image,(2,0,1))
        street_image_name=os.path.join(self.root_dir,self.data_frame.iloc[idx,1])
        street_image=io.imread(street_image_name)
        street_image=Image.fromarray((street_image[:,:,:3]))
# CHANGE street_image to convert RGBA to RGB
        # street_image=np.transpose(street_image,(2,0,1))
        

        if self.transform:
            aerial_image=self.transform[0](aerial_image)
            street_image=self.transform[1](street_image)
        batch={'aerial_image':aerial_image,'street_image':street_image}
        # 
        return batch


data_transforms = {
    'aerial_image': transforms.Compose([
        transforms.Resize((224,224)),
        transforms.ToTensor()
        ,transforms.Normalize([.3802,.3770,.3493], [.1999,.1997,.1928])
    ]),
    'street_image': transforms.Compose([
        transforms.Resize(640,640),
        transforms.CenterCrop(640),
        transforms.ToTensor()
        
    ]),
}

data_train=AerialDataset('../aerial2/data_al_train.csv','..',transform=[data_transforms['aerial_image'],    transforms.Compose([transforms.Resize((224,448)),transforms.ToTensor(),transforms.Normalize([.4881,.5005,.5113], [.1888,.1843,.1968])])])
# a,b,c,d=calc_mean_std(data_train)

data_test=AerialDataset('../aerial2/data_al_test.csv','..',transform=[data_transforms['aerial_image'],transforms.Compose([transforms.Resize((224,448)),transforms.ToTensor(),transforms.Normalize([.4881,.5005,.5113], [.1888,.1843,.1968])])])
data_test_actual=AerialDataset('../aerial2/data_al_test.csv','..',transform=[data_transforms['aerial_image'],transforms.Compose([transforms.Resize((224,448)),transforms.ToTensor(),transforms.Normalize([.4881,.5005,.5113], [.1888,.1843,.1968])])])
dataLoader_train=torch.utils.data.DataLoader(data_train,batch_size=batch_size,shuffle=True,num_workers=4)
dataLoader_test=torch.utils.data.DataLoader(data_test,batch_size=batch_size,shuffle=True,num_workers=4)

dataLoader_actual_test=torch.utils.data.DataLoader(data_test_actual,batch_size=batch_size,shuffle=True,num_workers=4)
dataloaders={'train':dataLoader_train,'val':dataLoader_test}




use_gpu = torch.cuda.is_available()


def imshow(inp, title=None):
    """Imshow for Tensor."""
    inp = inp.numpy().transpose((1, 2, 0))
    mean = np.array([0.485, 0.456, 0.406])
    std = np.array([0.229, 0.224, 0.225])
    inp = std * inp + mean
    inp = np.clip(inp, 0, 1)
    plt.imshow(inp)
    if title is not None:
        plt.title(title)
    plt.pause(3)  # pause a bit so that plots are updated


# Get a batch of training data
# inputs, classes = next(iter(dataloaders['train']))

# # Make a grid from batch
# out = torchvision.utils.make_grid(inputs)

# imshow(out, title=[class_names[x] for x in classes])


######################################################################
# Training the model
# ------------------
#
# Now, let's write a general function to train a model. Here, we will
# illustrate:
#
# -  Scheduling the learning rate
# -  Saving the best model
#
# In the following, parameter ``scheduler`` is an LR scheduler object from
# ``torch.optim.lr_scheduler``.

# print(len(data_train))
# 
def train_model(model,criterion, optimizer, scheduler, num_epochs=50):
    since = time.time()

    best_model_wts = copy.deepcopy(model.state_dict())
    best_acc = 99999.
    iters=0
    flag=1
    for epoch in range(num_epochs):
        print('Epoch {}/{}'.format(epoch, num_epochs - 1))
        print('-' * 10)

        # Each epoch has a training and validation phase
        for phase in ['train', 'val']:
            if phase == 'train':
                scheduler.step()
                model.train(True)  # Set model to training mode
            else:

                model.eval()  # Set model to etestuate mode

            running_loss = 0.0
            running_corrects = 0
            running_loss_val=0
            iters=0
            k=0
            epoch_loss=0
            epoch_loss_val=0
#Part of actual validation code            
            # aerial_list=[]
            # street_list=[]
            # weights1=np.
            # Iterate over data.
            for i,data in enumerate(dataloaders[phase]):
                # get the inputs
                inputs, labels = data['aerial_image'],data['street_image']
                # print(inputs.shape,labels.shape)
                print(i)
                # wrap them in Variablex
                # print(use_gpu)
                if use_gpu:
                    inputs = Variable(inputs.cuda())
                    
                    
                    labels = Variable(labels.cuda())
                    # if(flag==1):
                    #     # c,d=model(inputs,labels)
                    #     # writer.add_graph_onnx(model)
                    #     c,d=model(inputs,labels)
                    #     k=make_dot(c,params=dict(model.named_parameters()))
                    #     io.imsave('hel.png',k)
                    #     a=input('cont?')
                    #     flag=0
                else:
                    inputs, labels = Variable(inputs), Variable(labels)

                if(inputs.shape[0]!=batch_size):
                    continue
                # zero the parameter gradients
                optimizer.zero_grad()

                # forward
                outputs,outputs2 = model(inputs,labels)
                # print('aerial outputs {}'.format(outputs))
# 
                # a=input('dfgdfg')

                # print('streeet outputs {}'.format(outputs2))
                # a=input('dfgdfg')
                loss=criterion(outputs,outputs2)
                # statistics
                iteration_loss=0
                iteration_loss=loss.data[0]
                running_loss += iteration_loss
                
                # running_corrects += torch.sum(preds == labels.data)

                print('Loss: {}'.format(iteration_loss))
                print('running loss {}'.format(running_loss))

                # backward + optimize only if in training phase
                if phase == 'train':
                    iters=len(dataloaders[phase])*epoch
                    loss.backward()
                    torch.nn.utils.clip_grad_norm(model.parameters(),2.)
                    optimizer.step()
                    epoch_loss = running_loss / len(data_train)
                    writer.add_scalar('data/loss',iteration_loss,i+iters)
                else:
                    running_loss_val+=loss.data[0]
                    iters=len(dataloaders[phase])*epoch
                    epoch_loss_val = running_loss_val / len(data_test_actual)
                    writer.add_scalar('data/loss_val',iteration_loss,i+iters)
                    # aerial_list.append(outputs)
                    # street_list.append(outputs2)



           
                     
            
            # epoch_acc = running_corrects / dataset_sizes[phase]


            print(running_loss)
            print('hee')
            print(iteration_loss)

            # deep copy the model
            if phase=='val' and epoch_loss_val < best_acc:
                writer.add_scalar('data/epoch_loss_val',epoch_loss_val,iters+len(data_test_actual))
                best_acc = epoch_loss_val
                # best_model_wts = copy.deepcopy(model.state_dict())
                torch.save(model,'./nathans_model_pval_vgg3.pth')
                print('{} Loss: {} Acc: '.format(
                    phase, epoch_loss_val))                          
            else:
                writer.add_scalar('data/epoch_loss',epoch_loss,iters+len(data_train))
                
                # for name,param in model.named_parameters():


                #     writer.add_histogram(name,param,iters+len(data_train))            
                print('{} Loss: {} Acc: '.format(
                    phase, epoch_loss))                
                    
    writer.export_scalars_to_json('./all_scalars.json')
    writer.close()
    
 #Actual validation code (don't foregt the aerial_list,street_list above)

            # if phase=='val' and len(aerial_list)==len(dataloaders['val']):
            #     # with torch.cpu():
            #     print('cont?')
            #     a=input()
            #     aerial_matrix=torch.stack(aerial_list,dim=0).cpu()
            #     street_matrix=torch.stack(street_list,dim=0).cpu()
            #     distance_matrix=pairwise_distances(aerial_matrix,street_matrix)
            #     idx=distance_matrix.min(dim=1)
            #     for i in range(0,len(idx[1])):
            #         print(idx[1][i],i)
            #         # a=input(('cont?'))
            #         if(idx[1][i]==i):
            #             print(i,idx[1][i])
            #             correct+=1

            #     print('correct ones {}'.format(correct))
            #     print('total number {}'.format(len(idx[1])))
            #     print('top-1 accuracy {}'.format(correct/len(idx[1])))  
            #     aerial_list=[]
            #     street_list=[]              
            #     if (best_acc<(correct/len(idx[1]))):
            #         best_acc = correct/len(idx[1])
            #         best_model_wts =copy.deepcopy(model.state_dict())

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(
        time_elapsed // 60, time_elapsed % 60))
    # print('Best val Acc: {:4f}'.format(best_acc))

    # load best model weights
    # best_model_wts=copy.deepcopy(model.state_dict())
    # model.load_state_dict(best_model_wts)
    model.load_state_dict(best_model_wts)

    return model
# with torch.cuda.device(1):

# path='whole_resnet18_places365_python36.pth.tar'
# # visualize_model(model_ft)
# model_ftq=torch.load(path)
# params={i:j for i,j in model_ftq.named_parameters() }
# # print(params.keys())
# paramsi=dict(params)
# for i in paramsi.keys():
#     if('fc' in i):
#         del params[i]


#Pass params when possible to load places' weights
# model=nathans_model(batch_size=batch_size)
model=torch.load('./nathans_model_pval_vgg3.pth')

model=model.cuda()
repr(model)

criterion=HuberLoss()

# Observe that all parameters are being optimized
# print({ i:j for i,j in model.named_parameters()}.keys())
for i,p in enumerate(model.parameters()):
            p.bias=True
            print(i)

optimizer_ft = optim.RMSprop(model.parameters(), lr=0.0001, momentum=0.9,weight_decay=.0005)

# Decay LR by a factor of 0.1 every 7 epochs
exp_lr_scheduler = lr_scheduler.StepLR(optimizer_ft, step_size=100, gamma=0.1)

######################################################################
# Train and evaluate
# ^^^^^^^^^^^^^^^^^^
#
# It should take around 15-25 min on CPU. On GPU though, it takes less than a
# minute.
#


model_ft=model
# print(model.modules())
dicts={i:j for i,j in model.named_parameters()}
# print(dicts)
# print(dicts.keys())
# print(model_ft.layer4)
criterion=HuberLoss()

model_ft = train_model(model, criterion, optimizer_ft, exp_lr_scheduler,
                       num_epochs=10)




torch.save(model_ft,'./nathans_model_pval_vgg3.pth')
model_ft.eval()

running_loss=0
for i,data in enumerate(dataLoader_actual_test):
    inputs,targets=data['aerial_image'],data['street_image']
    if(use_gpu==True):
        inputs=Variable(inputs.cuda())
        targets=Variable(targets.cuda())
    outputs1,outputs2=model_ft(inputs,targets)
    
#REPLACE huber_loss with l2 distance   
    
    # outputs1=outputs1.unsqueeze(dim=1)
    # outputs2=outputs2.unsqueeze(dim=1)
    
    # print(outputs1.shape,outputs2.shape)
    
    outputs1=outputs1.view((outputs1.shape[0],outputs1.shape[1]*outputs1.shape[2]) )
    outputs2=outputs2.view((outputs2.shape[0],outputs2.shape[1]*outputs2.shape[2]))
    # outputs1=torch.unsqueeze(outputs1,dim=1)
    # outputs2=torch.unsqueeze(outputs2,dim=1)
    loss=Variable(torch.FloatTensor(1,batch_size).zero_())
    
    # for i in range(0,batch_size):
    #     print(outputs1[i].shape,outputs2[i].shape)
    #     loss[i]=F.pairwise_distance(outputs1[i],outputs2[i])
    # print(outputs1.shape,outputs2.shape)
    loss=F.pairwise_distance(outputs1,outputs2)
    print(loss.data)

    

    iteration_loss=loss.data[0]*batch_size
    print(iteration_loss.shape)
    
    running_loss+=iteration_loss

    # print('loss: {:.4f}'.format(iteration_loss))

    # print(i)
    

print(running_loss/len(data_test_actual))
# print('epoch loss_test :{:.4f}'.format(running_loss/len(data_test_actual)))

######################################################################
#
sys.exit(0)

visualize_model(model_ft)


######################################################################
# ConvNet as fixed feature extractor
# ----------------------------------
#
# Here, we need to freeze all the network except the final layer. We need
# to set ``requires_grad == False`` to freeze the parameters so that the
# gradients are not computed in ``backward()``.
#
# You can read more about this in the documentation
# `here <http://pytorch.org/docs/notes/autograd.html#excluding-subgraphs-from-backward>`__.
#

model_conv = torchvision.models.resnet18(pretrained=True)
for param in model_conv.parameters():
    param.requires_grad = False

# Parameters of newly constructed modules have requires_grad=True by default
num_ftrs = model_conv.fc.in_features
model_conv.fc = nn.Linear(num_ftrs, 2)

if use_gpu:
    model_conv = model_conv.cuda()

criterion = nn.CrossEntropyLoss()

# Observe that only parameters of final layer are being optimized as
# opoosed to before.
optimizer_conv = optim.RMSprop(model_conv.fc.parameters(), lr=0.001, momentum=0.9)

# Decay LR by a factor of 0.1 every 7 epochs
exp_lr_scheduler = lr_scheduler.StepLR(optimizer_conv, step_size=7, gamma=0.1)


######################################################################
# Train and evaluate
# ^^^^^^^^^^^^^^^^^^
#
# On CPU this will take about half the time compared to previous scenario.
# This is expected as gradients don't need to be computed for most of the
# network. However, forward does need to be computed.
#

model_conv = train_model(model_conv, criterion, optimizer_conv,
                         exp_lr_scheduler, num_epochs=10)

######################################################################
#

visualize_model(model_conv)

plt.ioff()
plt.show()
