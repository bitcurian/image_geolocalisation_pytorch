

from __future__ import print_function, division

import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
from torch.autograd import Variable
import numpy as np
import torchvision
from torchvision import datasets, models, transforms
from torch.utils.data import DataLoader,Dataset
import matplotlib.pyplot as plt
import time
import os
import copy
import sys
from skimage import io,transform
import pandas as pd
from PIL import Image
import os
import torch
import pandas as pd
from skimage import io, transform
import numpy as np
import matplotlib.pyplot as plt
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
import vgg
from logger import Logger
plt.ion()   # interactive mode


class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, [3,3])
#ADJUST  with max pooling for now        
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, [3,3])
        self.pool=nn.MaxPool2d(2,2)
        self.conv3=nn.Conv2d(16,4,[3,3])

    def forward(self, x):
        x = self.pool(F.selu(self.conv1(x)))
        x = self.pool(F.selu(self.conv2(x)))
        x=F.selu(self.conv3(x))

        x = x.view(-1, 16 * 5 * 5)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x


'B': [64, 64, 'M', 128, 128, 'M', 256, 256, 'M', 512, 512, 'M', 512, 512, 'M'],
class vgg11_skipup(nn.Module):
    def__init__(self):
        super(vgg11_skip[,self]).__init__()
        self.conv1=nn.Conv2d(3,64,kernel_size=[3,3])
        self.conv2=nn.Conv2d(64,64,kernel_size=[3,3])
        self.conv3=nn.Conv2d(64,128,kernel_size=[3,3])
        self.conv4=nn.Conv2d(128,128,kernel_size=[3,3])
        self.conv5=nn.Conv2d(128,128,kernel_size=[3,3])
        self.conv6=nn.Conv2d(128,256,kernel_size=[3,3])
        self.conv7=nn.Conv2d(256,256,kernel_size=[3,3])
        self.conv8=nn.Conv2d(256,512,kernel_size=[3,3])
        self.conv9=nn.Conv2d(512,512,kernel_size=[3,3])
        self.conv10=nn.Conv2d(512,512,kernel_size=[3,3])
        self.conv11=nn.Conv2d(512,512,kernel_size=[3,3])



    def forward(self,inputs):


class SiameseNetwork(nn.Module):
    def __init__(self):
        super(SiameseNetwork, self).__init__()
        self.cnn1 = nn.Sequential(
            nn.Conv2d(1, 20, kernel_size=5),
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(20),
            
            nn.MaxPool2d(2, stride=2),
            nn.Conv2d(20, 50, kernel_size=5),
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(50),
            
            nn.MaxPool2d(2, stride=2))

        self.fc1 = nn.Sequential(
            nn.Linear(50 * 4 * 4, 500),
            nn.ReLU(inplace=True),
            nn.Linear(500, 10),
            nn.Linear(10, 2))

    def forward_once(self, x):
        output = self.cnn1(x)
        output = output.view(output.size()[0], -1)
        output = self.fc1(output)
        return output

    def forward(self, input1, input2):
        output1 = self.forward_once(input1)
        output2 = self.forward_once(input2)
        return output1, output2

# net=SiameseNetwork()
#SET mean testues for RGB
class SiameseVGG11(nn.Module):
    def __init__(self):
        super(SiameseVGG11, self).__init__()
        self.vgg11_bn_up=vgg.vgg11_bn(pretrained=True)
        self.vgg11_bn_down=vgg.vgg11_bn(pretrained=True)


    def forward(self, input1, input2):
        output1 = self.vgg11_bn_up(input1)
        output2 = self.vgg11_bn_down(input2)
        return output1, output2    
vgg11_bn_up=vgg.vgg11_bn(pretrained=True)
vgg11_bn_down=vgg.vgg11_bn(pretrained=True)

data_transforms = {
    'aerial_image': transforms.Compose([
        transforms.Resize((224,224)),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ]),
    'street_image': transforms.Compose([
        transforms.Resize(64),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ]),
}

# data_dir = 'aerial'
# image_datasets = {x: datasets.ImageFolder(os.path.join(data_dir, x),
#                                           data_transforms[x])
#                   for x in ['train', 'test']}
# dataloaders = {x: torch.utils.data.DataLoader(image_datasets[x], batch_size=4,
#                                              shuffle=True, num_workers=4)
#               for x in ['train', 'test']}
# dataset_sizes = {x: len(image_datasets[x]) for x in ['train', 'test']}
# class_names = image_datasets['train'].classes
class AerialDataset(Dataset):
    """Face Landmarks dataset."""

    def __init__(self, csv_file, root_dir, transform=None):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a batch.
        """
        self.data_frame = pd.read_csv(csv_file)
        self.root_dir = root_dir
        self.transform = transform

    def __len__(self):
        return len(self.data_frame)

    def __getitem__(self, idx):
        aerial_img_name = os.path.join(self.root_dir,
                                self.data_frame.iloc[idx, 0])
        aerial_image = io.imread(aerial_img_name)
        aerial_image=Image.fromarray(np.uint8(aerial_image))
        # aerial_image=np.transpose(aerial_image,(2,0,1))
        street_image_name=os.path.join(self.root_dir,self.data_frame.iloc[idx,1])
        street_image=io.imread(street_image_name)
        street_image=Image.fromarray((street_image[:,:,:3]))
# CHANGE street_image to convert RGBA to RGB
        # street_image=np.transpose(street_image,(2,0,1))
        

        if self.transform:
            aerial_image=self.transform[0](aerial_image)
            street_image=self.transform[1](street_image)
        batch={'aerial_image':aerial_image,'street_image':street_image}
        # sys.exit(0)
        return batch


data_train=AerialDataset('./aerial/data_op_training.csv','aerial',transform=[data_transforms['aerial_image'],transforms.Compose([transforms.Resize((224,1232)),transforms.ToTensor()]),transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])])
data_test=AerialDataset('./aerial/data_op_test.csv','aerial',transform=[data_transforms['aerial_image'],transforms.Compose([transforms.Resize((64,64)),transforms.ToTensor()]),transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])])

dataLoader_train=torch.utils.data.DataLoader(data_train,batch_size=4,shuffle=True,num_workers=4)
dataLoader_test=torch.utils.data.DataLoader(data_test,batch_size=4,shuffle=True,num_workers=4)
dataloaders={'train':dataLoader_train,'test':dataLoader_test}

use_gpu = torch.cuda.is_available()

######################################################################
# Visualize a few images
# ^^^^^^^^^^^^^^^^^^^^^^
# Let's visualize a few training images so as to understand the data
# augmentations.

def imshow(inp, title=None):
    """Imshow for Tensor."""
    inp = inp.numpy().transpose((1, 2, 0))
    mean = np.array([0.485, 0.456, 0.406])
    std = np.array([0.229, 0.224, 0.225])
    inp = std * inp + mean
    inp = np.clip(inp, 0, 1)
    plt.imshow(inp)
    if title is not None:
        plt.title(title)
    plt.pause(0.001)  # pause a bit so that plots are updated



def train_model(model1,model2, criterion, optimizer, scheduler, num_epochs=25):
    since = time.time()
    step-
    # best_model_wts = copy.deepcopy(model.state_dict())
    # best_acc = 0.0

    for epoch in range(num_epochs):
        print('Epoch {}/{}'.format(epoch, num_epochs - 1))
        print('-' * 10)

        # Each epoch has a training and testidation phase
        for phase in ['train', 'test']:
            if phase == 'train':
                scheduler.step()
                model1.train(True)  # Set model to training mode
                model2.train(True)
            else:
                model1.train(False)  # Set model to etestuate mode
                model2.train(False)  # Set model to etestuate model

            running_loss = 0.0
            running_corrects = 0

            # Iterate over data.
            for data in dataloaders[phase]:
                # get the inputs
                aerial,street  = data['aerial_image'],data['street_image']
                # print(aerial.shape)

                # print(data)
                # sys.exit(0)
                # wrap them in Variable
                if use_gpu:
                    # print(inputs)
                    # # sys.exit(0)
                    aerial = Variable(aerial.cuda())
                    street = Variable(street.cuda())
                else:
                    aerial, street = Variable(aerial), Variable(street)

                # zero the parameter gradients
                optimizer.zero_grad()

                # forward
                outputs1 = model1(aerial)
                outputs2=model2(street)
                
                loss = criterion(outputs1, outputs2)

                # backward + optimize only if in training phase
                if phase == 'train':
                    loss.backward()
                    optimizer.step()

                # statistics
                running_loss += loss.data[0] * aerial.size(0)
                if (step+1) % 100 == 0:
                    print ('Step [%d/%d], Loss: %.4f, Acc: %.2f' 
                       %(step+1, total_step, loss.data[0], accuracy.data[0]))

                #==== ======== TensorBoard logging ============#
                # (1) Log the scalar values
                    info = {
                    'loss': loss.data[0],
                    'accuracy': accuracy.data[0]
                    }

                    for tag, value in info.items():
                        logger.scalar_summary(tag, value, step+1)

                # (2) Log values and gradients of the parameters (histogram)
                    for tag, value in net.named_parameters():
                        tag = tag.replace('.', '/')
                        logger.histo_summary(tag, to_np(value), step+1)
                        logger.histo_summary(tag+'/grad', to_np(value.grad), step+1)

                # (3) Log the images
                    info = {
                    'images': to_np(images.view(-1, 28, 28)[:10])
                        }

                    for tag, images in info.items():
                        logger.image_summary(tag, images, step+1)
                # running_corrects += torch.sum(preds == labels.data)
                # euc=loss.data[0]*aerial.size[0]
            epoch_loss = running_loss / dataset_sizes[phase]
            # epoch_acc = running_corrects / dataset_sizes[phase]
            # epoch_euc=
            print('{} Loss: {:.4f} Euclidean distance: {:.4f}'.format(
                phase, epoch_loss))

            # deep copy the model
            # if phase == 'test' and epoch_acc > best_acc:
            #     best_acc = epoch_acc
            #     best_model_wts = copy.deepcopy(model.state_dict())

        print()

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(
        time_elapsed // 60, time_elapsed % 60))
    print('Best test Acc: {:4f}'.format(best_acc))

    # load best model weights
    # model.load_state_dict(best_model_wts)
    return model1.state_dict(),model2.state_dict()


######################################################################
# Visualizing the model predictions
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#
# Generic function to display predictions for a few images
#

def visualize_model(model, num_images=6):
    images_so_far = 0
    fig = plt.figure()

    for i, data in enumerate(dataloaders['test']):
        aerial, labels = data
        if use_gpu:
            aerial, labels = Variable(aerial.cuda()), Variable(labels.cuda())
        else:
            aerial, labels = Variable(aerial), Variable(labels)

        outputs = model(aerial)
        _, preds = torch.max(outputs.data, 1)

        for j in range(aerial.size()[0]):
            images_so_far += 1
            ax = plt.subplot(num_images//2, 2, images_so_far)
            ax.axis('off')
            ax.set_title('predicted: {}'.format(class_names[preds[j]]))
            imshow(aerial.cpu().data[j])

            if images_so_far == num_images:
                return

######################################################################
# Finetuning the convnet
# ----------------------
#
# Load a pretrained model and reset final fully connected layer.
#

# model_ft=net
# model_ft = models.resnet18(pretrained=True)
# num_ftrs = model_ft.fc.in_features
# model_ft.fc = nn.Linear(num_ftrs, 2)

#SET UP model with pretrained weights -partial
# net=vgg.vgg11_bn(pretrained=True)
# net=SiameseVGG11()
if use_gpu:
    vgg11_bn_up=vgg.vgg11_bn(pretrained=True,padding='same').cuda()

    vgg11_bn_down=vgg.vgg11_bn(pretrained=True,padding='same').cuda()

# criterion = nn.CrossEntropyLoss()
# criterion =nn.MSELoss()
criterion=nn.SmoothL1Loss()
params=list(vgg11_bn_up.parameters())+list(vgg11_bn_down.parameters())
# Observe that all parameters are being optimized
optimizer_ft = optim.SGD(params, lr=0.001, momentum=0.9)

# Decay LR by a factor of 0.1 every 7 epochs
exp_lr_scheduler = lr_scheduler.StepLR(optimizer_ft, step_size=7, gamma=0.1)

######################################################################
# Train and etestuate
# ^^^^^^^^^^^^^^^^^^
#
# It should take around 15-25 min on CPU. On GPU though, it takes less than a
# minute.
#

net1,net2 = train_model(vgg11_bn_up,vgg11_bn_down, criterion, optimizer_ft, exp_lr_scheduler,
                       num_epochs=25)


def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        m.weight.data.normal_(0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        m.weight.data.normal_(1.0, 0.02)
        m.bias.data.fill_(0)
######################################################################
#

# visualize_model(model_ft)


######################################################################
# ConvNet as fixed feature extractor
# ----------------------------------
#
# Here, we need to freeze all the network except the final layer. We need
# to set ``requires_grad == False`` to freeze the parameters so that the
# gradients are not computed in ``backward()``.
#
# You can read more about this in the documentation
# `here <http://pytorch.org/docs/notes/autograd.html#excluding-subgraphs-from-backward>`__.
#

# model_conv = torchvision.models.resnet18(pretrained=True)
# for param in model_conv.parameters():
#     param.requires_grad = False

# # Parameters of newly constructed modules have requires_grad=True by default
# num_ftrs = model_conv.fc.in_features
# model_conv.fc = nn.Linear(num_ftrs, 2)

# if use_gpu:
#     model_conv = model_conv.cuda()

# criterion = nn.CrossEntropyLoss()

# # Observe that only parameters of final layer are being optimized as
# # opoosed to before.
# optimizer_conv = optim.SGD(model_conv.fc.parameters(), lr=0.001, momentum=0.9)

# # Decay LR by a factor of 0.1 every 7 epochs
# exp_lr_scheduler = lr_scheduler.StepLR(optimizer_conv, step_size=7, gamma=0.1)


# ######################################################################
# # Train and etestuate
# # ^^^^^^^^^^^^^^^^^^
# #
# # On CPU this will take about half the time compared to previous scenario.
# # This is expected as gradients don't need to be computed for most of the
# # network. However, forward does need to be computed.
# #

# model_conv = train_model(model_conv, criterion, optimizer_conv,
#                          exp_lr_scheduler, num_epochs=25)

# ######################################################################
# #

# visualize_model(model_conv)
print('whut')
plt.ioff()
plt.show()
