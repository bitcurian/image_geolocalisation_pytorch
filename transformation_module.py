import sys
import torch
import torch.nn as nn
from torch.autograd import  Variable

import torch.nn.functional as F
import numpy as np

class op_hw(nn.Module):
	def __init__(self,height,width):
		super(op_hw,self).__init__()
		self.height=height
		self.width=width
		self.op_hw=torch.Tensor(self.height,self.width)
		self.opb_hw=torch.Tensor(self.height,self.width)
		self.op_hw=nn.init.kaiming_normal(self.op_hw,mode='fan_out')
		self.opb_hw=nn.init.kaiming_normal(self.opb_hw,mode='fan_out')
		self.op_hw=nn.Parameter(self.op_hw.cuda())
		self.opb_hw=nn.Parameter(self.opb_hw.cuda())

	def forward(self,x):
		x1=F.relu(x*self.op_hw.expand_as(x)+self.opb_hw.expand_as(x))
		return x1

class op_hd(nn.Module):
	def __init__(self,depth,height,width):
		super(op_hd,self).__init__()
		self.height=height
		self.width=width
		self.depth=depth
		self.op_hd=torch.Tensor(self.depth,self.height,self.width)
		self.opb_hd=torch.Tensor(self.depth,self.height,self.width)
		self.op_hd=nn.init.kaiming_normal(self.op_hd,mode='fan_out')
		self.opb_hd=nn.init.kaiming_normal(self.opb_hd,mode='fan_out')
		self.op_hd=nn.Parameter(self.op_hd.cuda())
		self.opb_hd=nn.Parameter(self.opb_hd.cuda())

	def forward(self,x):
		x1=F.relu(x*self.op_hd.expand_as(x)+self.opb_hd.expand_as(x))
		return x1

class op_dw(nn.Module):
	def __init__(self,depth,height,width):
		super(op_dw,self).__init__()
		self.height=height
		self.width=width
		self.depth=depth
		self.op_dw=torch.Tensor(self.depth,self.height,self.width)
		self.opb_dw=torch.Tensor(self.depth,self.height,self.width)
		self.op_dw=nn.init.kaiming_normal(self.op_dw,mode='fan_out')
		self.opb_dw=nn.init.kaiming_normal(self.opb_dw,mode='fan_out')
		self.op_dw=nn.Parameter(self.op_dw.cuda())
		self.opb_dw=nn.Parameter(self.opb_dw.cuda())

	def forward(self,x):
		x1=F.relu(x*self.op_dw.expand_as(x)+self.opb_dw.expand_as(x))
		return x1





class transfnet(nn.Module):
	def __init__(self,source_size,target_size,batch_size,mod=False):



		'''
		source_size: list[] H,W
		target_size: list[] H_,W_
		batch_size: int
		net: last convolution layer of resnet 18
		'''
		super(transfnet,self).__init__()
		self.mod=mod

		self.source_size=source_size
		self.target_size=target_size
		self.batch_size=batch_size
		if(self.mod==True):
			h=self.source_size[0]*self.source_size[1]
			w=self.target_size[0]*self.target_size[1]
			d=h+4		
			self.op_hw=op_hw(h,w)

			self.op_hd=op_hd(d,h,1)
			self.op_dw=op_dw(d,1,w)		
		# self.net=net
#4+self.source_size**2 is the length of the vectore to be tiled/expand()'d		
		self.conv1t=nn.Sequential(nn.Conv2d(4+(self.source_size[0]*self.source_size[1]),128,kernel_size=(1,1)),nn.BatchNorm2d(128),nn.ReLU())
		
		# self.conv2=nn.Sequential(nn.Conv2d(256,128,kernel_size=(1,1)),nn.BatchNorm2d(128),nn.ReLU())

		self.conv2t=nn.Sequential(nn.Conv2d(128,64,kernel_size=(1,1)),nn.BatchNorm2d(64),nn.ReLU())
		self.convL3t=nn.Conv2d(64,1,kernel_size=(1,1))

		self.conv1s=nn.Sequential(nn.Conv2d(512,64,kernel_size=(1,1)),nn.BatchNorm2d(64
			),nn.ReLU())
		
		print('werke')

		self.convL2s=nn.Conv2d(64,1,kernel_size=(1,1))





		# self.op_hw=torch.randn((h,w))
		# self.op_hd=torch.randn((d,h,1))
		# self.op_dw=torch.randn((d,1,w))
#biases
		# self.opb_hw=torch.randn((h,w))
		# self.opb_hd=torch.randn((d,h,1))
		# self.opb_dw=torch.randn((d,1,w))

		# self.op_hw=nn.Parameter(self.op_hw.cuda())
		# self.op_hd=nn.Parameter(self.op_hd.cuda())
		# self.op_dw=nn.Parameter(self.op_dw.cuda())
		# self.opb_hw=nn.Parameter(self.opb_hw.cuda())
		# self.opb_hd=nn.Parameter(self.opb_hd.cuda())
		# self.opb_dw=nn.Parameter(self.opb_dw.cuda())




	def compute_transfnet(self,net):

		# if(self.mod==True):
		# 	h=self.source_size[0]*self.source_size[1]
		# 	w=self.target_size[0]*self.target_size[1]		
		# 	d=h+4
		# 	self.op_hw=op_hw(h,w)
		# 	self.op_hd=op_hd(d,h,1)
		# 	self.op_dw=op_dw(d,1,w)		


		indexing_tensor=self.compute_index()
		print(indexing_tensor.shape)

		# h=torch.rand((5,4))
		# h1=Variable(h)
		indexing_tensor=Variable(indexing_tensor,requires_grad=False)
		# indexing_tensor=indexing_tensor.cuda()
		H,W=indexing_tensor.shape[2:]

		print('net shape is  {}'.format(net.shape))
		x=self.conv1s(net)
		indexing_tensor=indexing_tensor.repeat(x.shape[0],1,1,1)
		print(x.shape)
		x=self.convL2s(x)

		print(x.shape)
		

		x=x.view((x.shape[0],-1,1,1))
		print('reshaped shape is {}'.format(x.shape))
		x=x.repeat(1,1,H,W)

		print('tiled shape is {}'.format(x.shape))

		# x=x.view(self.batch_size,-1,H,W)

		# print('tiled shape is {}'.format(x.shape))
#IS x.view() required above
		
	
		print(x.shape,x.data.type())
		print(indexing_tensor.shape,type(indexing_tensor))
		out=torch.cat((x,indexing_tensor),dim=1)

		channels=out.shape[1]
# Transformation module operations : 3 ops 

		print(out.shape)
		if(self.mod==True):
			# with torch.cuda.device():
			# print(self.op_hw)


			# x1=F.relu(out*self.op_hw.expand_as(out)+self.opb_hw.expand_as(out))
			x1=self.op_hw(out)
			# x2=F.relu(x1*self.op_hd.expand_as(x1)+self.opb_hd.expand_as(x1))
			x2=self.op_hd(x1)
			x12=x1+x2
			x3=self.op_dw(x12)
			# x3=F.relu(x12*self.op_dw.expand_as(x12)+self.opb_dw.expand_as(x12))
			x=x3+x2+x1
			out=x


		x=self.conv1t(out)
		x=self.conv2t(x)
		x=self.convL3t(x)
		
		matrix=x.view((-1,self.source_size[0]*self.source_size[1],self.target_size[0]*self.target_size[1]))
		matrix=F.softmax(matrix,dim=2)

		self.matrix=matrix

		print('matrix dimensions are: {}'.format(matrix.shape))










#DImensionality reduction from last layer and add indexing

	def compute_index(self):

#calculate indexing 
		jj, ii = np.meshgrid(range(self.source_size[1]), range(self.source_size[0]), indexing='xy')
		xx, yy = np.meshgrid(range(self.target_size[1]), range(self.target_size[0]), indexing='xy')
		X, I = np.meshgrid(xx.flatten(), ii.flatten(), indexing='xy')
		Y, J = np.meshgrid(yy.flatten(), jj.flatten(), indexing='xy')

		# normalize to 0 and 1
		I = I.astype('float32') / (self.source_size[0]-1)
		J = J.astype('float32') / (self.source_size[1]-1)
		Y = Y.astype('float32') / (self.target_size[0]-1)
		X = X.astype('float32') / (self.target_size[1]-1)

		I,J,Y,X=torch.from_numpy(I),torch.from_numpy(J),torch.from_numpy(Y),torch.from_numpy(X)
		# I,J,Y,X=Variable(I,requires_grad=False).cuda(),Variable(J,requires_grad=False).cuda(),Variable(Y,requires_grad=False).cuda(),Variable(X,requires_grad=False).cuda()
		I,J,Y,X=I.cuda(),J.cuda(),Y.cuda(),X.cuda()
		indexing = torch.stack([I, J, Y, X], dim=0)

		return torch.unsqueeze(indexing, dim=0)		



	def transfnet(self,inputs):
# perform matrix multiplication
#inputs refers to the aerial aerial vector

		b,c,h,w=inputs.shape

		h_,w_=self.target_size


		aerial_vector=inputs.view((-1,c,h*w))
		print('aerial_vector dimensions {}'.format(aerial_vector.shape))
		print('matrix multiplication output {}'.format(self.matrix.shape))

		out=torch.matmul(aerial_vector,self.matrix)
		out=out.view((-1,c,h_,w_))
		print('aerial_vector dimensions {}'.format(aerial_vector.shape))
		print('matrix multiplication output {}'.format(out.shape))

		return out








	
